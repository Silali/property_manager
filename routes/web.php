<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('dashboard');
});

Route::get('/mailable', function () {
    $payment = App\Payment::find(1);
    $invoice = $payment->invoice;
    return new App\Mail\SendInvoice($invoice);
});

Auth::routes();

Route::group(['middleware' => ['auth']], function(){
    Route::get('dashboard', 'DashboardController@index')->name('home');

    Route::get('bills', 'BillController@index');
    Route::post('bills/clear/{id}', 'BillController@clear');

    Route::get('downloads/invoice/{id}', 'TenantController@downloadInvoice');
    Route::get('downloads/receipt/{id}', 'TenantController@downloadReceipt');

    Route::get('properties', 'PropertyController@index');
    Route::post('properties/create', 'PropertyController@store');
    Route::post('properties/update/{id}', 'PropertyController@update');
    Route::post('properties/delete/{id}', 'PropertyController@delete');
    Route::get('properties/get_projection_data/{id}', 'PropertyController@getChart');
    Route::get('floors/get_projection_data/{id}', 'FloorController@getChart');
    Route::get('properties/{id}/floors', 'FloorController@index');
    Route::post('properties/floors/update/{id}', 'FloorController@update');
    Route::post('properties/floors/store/{property_id}', 'FloorController@store');
    Route::post('properties/floors/delete/{id}', 'FloorController@delete');
    Route::get('properties/get_floors/{id}', 'FloorController@getFloors');
    Route::get('properties/floor_units/{floor_id}', 'FloorUnitController@index');
    Route::post('properties/floor_units/store/{floor_id}', 'FloorUnitController@store');
    Route::post('properties/floor_units/update/{id}', 'FloorUnitController@update');
    Route::get('properties/floor_units/get/{floor_id}', 'FloorUnitController@getUnits');

    Route::get('properties/parking_spots/{floor_id}', 'ParkingSpotController@index');
    Route::post('properties/parking_spot/store/{floor_id}', 'ParkingSpotController@store');
    Route::post('properties/parking_spot/update/{id}', 'ParkingSpotController@update');
    Route::get('properties/parking_spots/get/{property_id}', 'ParkingSpotController@getSpots');

    Route::get('tenants', 'TenantController@index')->middleware('filter.params');
    Route::get('tenants/create', 'TenantController@create');
    Route::post('tenants/store', 'TenantController@store');
    Route::get('tenants/view/{id}', 'TenantController@edit');
    Route::post('tenants/update/{id}', 'TenantController@update');
    Route::get('tenants/invoice/{id}', 'InvoiceController@index');
    Route::post('tenants/delete/{id}', 'TenantController@delete');
    Route::get('tenants/document/{id}', 'TenantController@getDocument');
    Route::get('tenants/documents/{id}', 'TenantController@showDocuments');
    Route::get('tenants/fix/documents', 'TenantDocumentController@store');
    Route::post('tenants/documents/upload/{id}/{tag}', 'TenantDocumentController@upload');
    Route::get('tenants/documents/download/{id}/{tag}', 'TenantDocumentController@getDocument');
    Route::get('tenants/statements/{tenant}', 'TenantStatementController@index');
    Route::post('tenants/electricity_bill/create/{id}', 'ElectricityBillController@store');
    Route::get('tenants/electricity_bill/get/{id}', 'ElectricityBillController@getBillInfo');

    Route::get('payments', 'PaymentController@index')->middleware('filter.params');
    Route::get('payments/get', 'PaymentController@show');
    Route::get('payments/invoices/clear/create/{invoice_id}', 'PaymentController@create');
    Route::post('payments/invoices/clear/store/{invoice_id}', 'PaymentController@store');
    Route::get('payments/invoices/view/{invoice_id}', 'InvoiceController@show');
    Route::get('payments/invoices/download/{invoice_id}', 'InvoiceController@download');
    Route::get('payments/invoice', 'InvoiceController@index');
    Route::get('payments/document/{id}', 'PaymentController@getDocument');


    Route::get('settings', 'SettingController@index')->middleware('filter.params');

    Route::get('settings/bills/get', 'BillController@getSettings');
    Route::post('settings/bills/create', 'BillController@storeSetting');
    Route::post('settings/bills/update/{id}', 'BillController@updateSetting');
    Route::post('settings/bills/delete/{id}', 'BillController@deleteSetting');

    Route::post('settings/interest_rate/set', 'InterestRateController@store');
    Route::get('settings/interest_rate/get', 'InterestRateController@get');
    Route::post('settings/service_charge', 'ServiceChargeController@store');
    Route::get('settings/get_service_charge', 'ServiceChargeController@get');

    Route::post('settings/id_documents/add', 'IdDocumentController@store');
    Route::get('settings/id_documents/get', 'IdDocumentController@get');
    Route::post('settings/id_documents/delete', 'IdDocumentController@delete');

    Route::post('settings/payment_terms/add', 'PaymentTermController@store');
    Route::get('settings/payment_terms/get', 'PaymentTermController@get');
    Route::post('settings/payment_terms/delete', 'PaymentTermController@delete');
    Route::post('settings/payment_terms/update', 'PaymentTermController@update');

    Route::post('settings/reports/create', 'ReportControler@store');
    Route::get('settings/reports/get', 'ReportControler@get');
    Route::post('settings/reports/update/{id}', 'ReportControler@update');
    Route::post('settings/reports/delete/{id}', 'ReportControler@delete');

    Route::get('users', 'UserController@index');
    Route::post('user/create', 'UserController@store');
    Route::post('users/deactivate/{id}', 'UserController@deactivate');
    Route::post('users/activate/{id}', 'UserController@activate');
});
