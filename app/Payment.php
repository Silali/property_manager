<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Payment extends Model
{
    use SoftDeletes, Mediable;

    protected $with = ['media'];

    protected $guarded = [];

    protected $appends = ['document_url'];

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function receipt()
    {
        return $this->hasOne(Receipt::class);
    }

    public function getDocumentUrlAttribute()
    {
        $urls = [];
        $appDomain = url('/');
        foreach ($this->media as $photo) {

            $disk = $photo->disk;
            $directory = $photo->directory;
            $filename = $photo->filename;
            $extension = $photo->extension;



            $url = $disk.'/'.$directory.'/'.$filename.'.'.$extension;

            array_push($urls, $url);
        }

        return $urls[0];
    }
}
