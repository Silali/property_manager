<?php

namespace App;

use App\Floor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $with = ['floors'];

    protected $appends = ['total_rentable_area', 'undesignated_area', 'total_common_area'];

    public function floors()
    {
    	return $this->hasMany(Floor::class);
    }

    public function units()
    {
        return $this->hasManyThrough(FloorUnit::class,  Floor::class);
    }

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }

    public function parking_spots()
    {
        return $this->hasManyThrough(ParkingSpot::class,  Floor::class);
    }

    public function getTotalRentableAreaAttribute()
    {
        $area =0;
        foreach ($this->floors as $floor) {
            $area += $floor->total_rentable_area;
        }
        return $area;
    }

    public function getUndesignatedAreaAttribute()
    {
        $area = 0;
        foreach ($this->floors as $floor) {
            $area += $floor->undesignated_area;
        }

        return $area;
    }

    public function getTotalCommonAreaAttribute()
    {
        return $this->floors()->sum('common_area');
    }
}
