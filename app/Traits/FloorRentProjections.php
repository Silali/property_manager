<?php

namespace App\Traits;

use App\Floor;
use App\Property;
use App\Setting;
use App\Tenant;
use Carbon\Carbon;

trait FloorRentProjections
{
    public function graph(Floor $floor)
    {
        $labels = [];
        $data = [];
        $start_year = Carbon::now();
        $end_year = Carbon::now()->addYears(10);
        $period = $end_year->diffInYears($start_year);

        for ($i = 0; $i < $period; $i ++) {
            $year = $start_year->addYears(1);
            $label = $year->format('Y');
            array_push($labels, (string)$label);
            $year_total_rent = 0;
            $property_rent = 0;
            $tenants = $floor->tenants;
//            dd($floor);
            if ($tenants != null) {
                foreach ($tenants as $tenant) {
                    $property_rent += $this->getAmountPayable($tenant, $i);
                }
            }
            $year_total_rent += $property_rent;
            array_push($data, $year_total_rent );
        }

        return array(
            'labels' => $labels,
            'datasets' => [
                'label' => "$floor->level floor rent projections",
                'data' => $data,
            ]
        );
    }

    private function getAmountPayable(Tenant $tenant, $year)
    {
        $service_charge = $this->getServiceCharge($tenant);
        //dump("service charge $service_charge");
//        \Log::info("Interest is: - $interest");
        //dump("interest $interest");
        $common_area_charge = $this->getCommonAreaCharge($tenant);
        $interest = $this->getInterest($tenant, $year, $common_area_charge);
//        \Log::info("Common area charge is: - $common_area_charge");
        //dump("ca charge $common_area_charge");
        $rent = $this->getFloorRent($tenant);
        //dump("rent $rent");
//        \Log::info("rent payable is - $rent");
        $amount_payable = ($rent + $service_charge + $common_area_charge + $interest);
        //dump("amount payable $amount_payable");
//        \Log::info("amount payable is: - $amount_payable");
        return $amount_payable * 12;
    }

    private function getInterest($tenant, $time,$ca_charge)
    {
        $principal = ($tenant->rent * $tenant->unit->area) + $ca_charge;
        $rate = $tenant->rate_of_contract->rate/100;
        return $principal * $rate * $time;
    }

    private function getCommonAreaCharge($tenant)
    {
        $tra = $tenant->unit->floor->total_rentable_area;
//        \Log::info("tra is $tra");
        $tca = $tenant->unit->floor->common_area;
//        \Log::info("tca is $tca");
        $ra = $tenant->unit->area;
//        \Log::info("ra is $ra");
        $ca = ($ra/$tra) * $tca;
//        \Log::info("ca is $ca");
        return  $ca * $tenant->ca_rent;
    }

    private function getFloorRent($tenant)
    {
        return $tenant->rent * $tenant->unit->area;
    }

    public function getServiceCharge($tenant)
    {
        $rate = Setting::where('key', 'service_charge')->first();
        if (! count($rate) > 0) {
            return 0;
        }

        return $rate->value * $tenant->floor->total_rentable_area;
    }
}
