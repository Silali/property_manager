<?php

namespace App\Traits;

use App\Invoice;
use App\Setting;
use App\Tenant;
use Carbon\Carbon;

trait HandlesInvoices
{
    public function createInvoice(Tenant $tenant)
    {
//        \Log::info("Service charge is: - $service_charge");
        $payment_term = $tenant->payment_term ? $tenant->payment_term: 'N/A';
//        \Log::info("Payment term is: - $payment_term");
        $floor = $tenant->unit->floor->level;

//        \Log::info("Interest is: - $interest");
        $common_area = $this->getCommonArea($tenant);
        $common_area_charge = $this->getCommonAreaCharge($tenant);
        $interest = $this->getInterest($tenant, $common_area_charge);
//        \Log::info("Common area charge is: - $common_area_charge");
        $rent = $this->getFloorRent($tenant);
//        \Log::info("rent payable is - $rent");
        $service_charge = $this->getServiceCharge($tenant);
        $parking_fees = $this->getParkingCharges($tenant);
        $electricity = $this->getElectricityInfo($tenant);
        $amount_payable = ($rent + $service_charge + $common_area_charge + $interest + $parking_fees + $electricity['bill']) * $payment_term->period;
//        \Log::info("amount payable is: - $amount_payable");
        $data = [
            'tenant_id' => $tenant->id,
            'property_name' => (bool) $tenant->property ? $tenant->property->name : 'N/A',
            'floor_level' => $floor,
            'unit_number' => (bool) $tenant->unit ? $tenant->unit->unit_number : 'N/A',
            'amount_payable' => $amount_payable,
            'payment_term' => $payment_term == 'N/A' ? 'N/A' : $payment_term->name,
            'rate_of_contract' => $tenant->rate_of_contract ? $tenant->rate_of_contract->rate : 'N/A',
            'period_of_contract' => $tenant->period_of_contract,
            'common_area_charge' => $common_area_charge,
            'interest' => $interest,
            'service_charge' => $service_charge,
            'common_area' => $common_area,
            'total_area' => $tenant->unit->area + $common_area,
            'rented_area' => $tenant->unit->area,
            'parking_spots' => $this->getParkingSpotsCount($tenant),
            'parking_charges' => $parking_fees,
            'rent' => $rent,
            'electricity_rate' => $electricity['rate'],
            'electricity_usage' => $electricity['usage'],
            'electricity_bill' => $electricity['bill'],
        ];

        $invoice = $tenant->invoices()->create($data);

        $balance = $invoice->amount_payable;
        $latest = $invoice->tenant->statement()->orderBy('id', 'desc')->first();
        if (count($latest) > 0) {
            $balance = $invoice->amount_payable + $latest->balance;
        }
        $statement = [
            'invoice_id' => $invoice->id,
            'date' => $invoice->created_at,
            'balance' => $balance,
            'credit' => $invoice->amount_payable
        ];

        $tenant->statement()->create($statement);

        return $invoice;
    }

    public function clearInvoice(Invoice $invoice, $payment_sum)
    {
        if ((int) $invoice->amount_payable <= (int) $payment_sum) {
            return $invoice->update([
                'cleared' => true
            ]);
        }
        return false;

    }

    public function getInterest($tenant, $ca_charge)
    {
       $start_of_lease = Carbon::parse($tenant->start_of_lease);
       $now = Carbon::now();
       $time = $now->diffInYears($start_of_lease);
       $principal = ($tenant->rent * $tenant->unit->area) + $ca_charge;
       $rate = $tenant->rate_of_contract->rate/100;

       return $principal * $rate * $time;
    }

    public function getCommonAreaCharge($tenant)
    {
        $tra = $tenant->unit->floor->total_rentable_area;
//        \Log::info("tra is $tra");
        $tca = $tenant->unit->floor->common_area;
//        \Log::info("tca is $tca");
        $ra = $tenant->unit->area;
//        \Log::info("ra is $ra");
        $ca = ($ra/$tra) * $tca;
//        \Log::info("ca is $ca");
        return  $ca * $tenant->ca_rent;
    }

    public function getCommonArea($tenant)
    {
        $tra = $tenant->unit->floor->total_rentable_area;
//        \Log::info("tra is $tra");
        $tca = $tenant->unit->floor->common_area;
//        \Log::info("tca is $tca");
        $ra = $tenant->unit->area;
//        \Log::info("ra is $ra");
        return ($ra/$tra) * $tca;
    }

    public function getFloorRent($tenant)
    {
        return $tenant->rent * $tenant->unit->area;
    }

    public function getServiceCharge($tenant)
    {
        $rate = Setting::where('key', 'service_charge')->first();
        if (! count($rate) > 0) {
            return 0;
        }

        return $rate->value * $tenant->floor->total_rentable_area;
    }

    public function getParkingSpotsCount($tenant)
    {
       return count($tenant->parking_spots);
    }

    public function getParkingCharges($tenant)
    {
        return $this->getParkingSpotsCount($tenant) * $tenant->parking_spot_rent;
    }

    public function getElectricityInfo($tenant)
    {
        $electricity = [];
        if ((bool)$e = $tenant->electricity_bill) {
            $electricity['rate'] = $e->current_rate;
            $electricity['usage'] = $e->current_usage - $e->previous_usage;
            $electricity['bill'] = $electricity['usage'] * $electricity['rate'];
        } else {
            $electricity['rate'] = 0;
            $electricity['bill'] = 0;
            $electricity['usage'] = 0;
        }

        return $electricity;
    }
}