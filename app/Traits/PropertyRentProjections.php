<?php

namespace App\Traits;

use App\Charts\PropertyOccupancyRate;
use App\Property;
use App\Setting;
use App\Tenant;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait PropertyRentProjections
{
    public function graph(Property $property, $service_charge_rate)
    {
        $labels = [];
        $data = [];

        $start_year = Carbon::now();
        $end_year = Carbon::now()->addYears(10);
        $period = $end_year->diffInYears($start_year);

        for ($i = 0; $i < $period; $i ++) {
            $year = $start_year->addYears(1);
            $label = $year->format('Y');
            array_push($labels, (string)$label);
            $year_total_rent = 0;
            $property_rent = 0;
            $tenants = $property->tenants;
            if ($tenants != null) {
                foreach ($tenants as $tenant) {
                    $property_rent += $this->getAmountPayable($tenant, $i, $service_charge_rate);
                }
            }
            $year_total_rent += $property_rent;
            array_push($data, $year_total_rent );
        }

        return array(
            'labels' => $labels,
            'datasets' => [
                'label' => "$property->name rent projections",
                'data' => $data,
            ]
        );
    }

    private function getAmountPayable(Tenant $tenant, $year, $service_charge_rate)
    {
        $service_charge = $this->getServiceCharge($tenant, $service_charge_rate);
        //dump("service charge $service_charge");
        //dump("interest $interest");
        $common_area_charge = $this->getCommonAreaCharge($tenant);
        \Log::info("Common area charge is: - $common_area_charge");
        //dump("ca charge $common_area_charge");
        $rent = $this->getFloorRent($tenant);
        //dump("rent $rent");
        $interest = $this->getInterest($tenant, $year, $common_area_charge);
        \Log::info("Interest is: - $interest");
        \Log::info("rent payable is - $rent");
        $parking_fees = $this->getParkingCharges($tenant);

        $amount_payable = ($rent + $service_charge + $common_area_charge + $interest + $parking_fees);
        //dump("amount payable $amount_payable");
        \Log::info("amount payable is: - $amount_payable");
        \Log::info("amount payable in year $year is: - ".$amount_payable*12);
        return $amount_payable * 12;
    }

    private function getInterest($tenant, $time, $ca_charge)
    {
        $principal = ($tenant->rent * $tenant->unit->area) + $ca_charge;
        $rate = $tenant->rate_of_contract->rate/100;
        return $principal * $rate * $time;
    }

    private function getCommonAreaCharge($tenant)
    {
        $floor = $tenant->unit->floor;
        $tra = $floor->total_rentable_area;
//        \Log::info("tra is $tra");
        $tca = $floor->common_area;
//        \Log::info("tca is $tca");
        $ra = $tenant->unit->area;
//        \Log::info("ra is $ra");
        $ca = ($ra/$tra) * $tca;
//        \Log::info("ca is $ca");
        return  $ca * $tenant->ca_rent;
    }

    private function getFloorRent($tenant)
    {
        return $tenant->rent * $tenant->unit->area;
    }

    private function getServiceCharge(Tenant $tenant, $rate)
    {
        return $rate * $tenant->floor->total_rentable_area;
    }

    private function getParkingSpotsCount($tenant)
    {
        return count($tenant->parking_spots);
    }

    private function getParkingCharges($tenant)
    {
        return $this->getParkingSpotsCount($tenant) * $tenant->parking_spot_rent;
    }
}
