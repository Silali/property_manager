<?php
/**
 * Created by PhpStorm.
 * User: silali
 * Date: 4/13/18
 * Time: 6:19 PM
 */

namespace App\Traits;


use App\Payment;
use App\Tenant;
use Faker\Provider\Image;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Plank\Mediable\Media;
use Plank\Mediable\MediaUploader;

trait HandlesDocumentUploads
{
    public function upload(Request $request, MediaUploader $mediaUploader, $id)
    {
        $payment = Payment::find($id);

        $validator = Validator::make($request->all(), [
            'file' => 'file'
        ]);
        // if there are validation errors, show that
        if ($validator->fails()) {
            return response(['message' => $validator->errors()], 433);
        }
        $file = $request->file('file');
        $folder = 'uploads/documents/payments/'.$this->getPaymentFolderName($payment->name).'/';
        $uniqid = uniqid();
        $mainFileName = $uniqid . '.' . $file->getClientOriginalExtension();
        $thumbFileName = $uniqid . '_thumb.' . $file->getClientOriginalExtension();

        if (!file_exists(public_path($folder))) {
            mkdir(public_path($folder), 0755, true);
        }
        $mainImage = Image::make($request->file('file'))
            ->resize(1080, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save(public_path($folder) . $mainFileName);
        // making the media entry
        $media = $mediaUploader->fromSource(public_path($folder) . $mainFileName)
            ->toDirectory($folder)
            ->upload();

        $payment->attachMedia($media, 'thumbnail');

        $thumbImage = Image::make($request->file('file'))
            ->resize(200, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
            ->save(public_path($folder) . $thumbFileName);
        return response()->json(['data' => $media], 201);
    }

    public function getPaymentFolderName($name)
    {
        return str_replace(' ', '_', strtolower(preg_replace("/[^ \w]+/", "", $name)));
    }

    public function getImages($id)
    {
        $place = Payment::find($id);
        return $place->getMedia('document')->sortBy('id');
    }

    public function deletePhotos(Request $request, $id)
    {
        $payment = Payment::find($id);
        $tenant = Tenant::find(1);
        $photoCount = count($request->photos);
        $folder = 'uploads/documents/payments/'.$this->getPaymentFolderName($tenant->name."_".$tenant->id).'/';
        foreach ($request->photos as $photo) {
            $media = Media::find($photo);
            $filepath = public_path($folder).$media->filename.".".$media->extension;
            $media->delete();
            if (file_exists($filepath)) {
                \File::delete($filepath);
            }
        }

        return [
            'status' => 'ok',
            'message' => "$photoCount photo(s) deleted successfully"
        ];
    }
}