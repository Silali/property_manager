<?php
/**
 * Created by PhpStorm.
 * User: silali
 * Date: 6/5/18
 * Time: 4:38 PM
 */

namespace App\Traits;


use App\FloorUnit;
use App\ParkingSpot;
use App\Tenant;
use Carbon\Carbon;

trait HandlesReport
{
    use HandlesInvoices;

    public function report()
    {
        $office_info = $this->getOfficesInfo();
        $report = [];

        $report['total_units'] = $office_info['total'];
        $report['unoccupied'] = $office_info['unoccupied'];
        $report['occupied'] = $office_info['occupied'];

        return $report;
    }

    public function getOfficesInfo()
    {
        $units = FloorUnit::all();
        $unoccupied = $units->filter(function($value, $key) {
            return $value->tenant_id == null;
        });


        return [
            'total' => count($units),
            'unoccupied' => count($unoccupied),
            'occupied' => count($units) - count($unoccupied)
        ];
    }

    public function getParkingSpotsInfo()
    {
        $spots = ParkingSpot::all();
        $unoccupied = $spots->filter(function($value, $index){
            return $value->tenant_id == null;
        });

        return [
            'total' => count($spots),
            'unoccupied' => count($unoccupied),
            'occupied' => count($spots) - count($unoccupied)
        ];
    }

    public function rent()
    {
        $total = 0;
        $paid = 0;
        $tenants = Tenant::all();
        $today = Carbon::now();
        foreach ($tenants as $tenant) {
            $total += $this->getTotalPayable($tenant);
            $currentMonth = date('m');
            $invoice = $tenant->invoices()->with('payments')->whereRaw('MONTH(created_at) = ?',[$currentMonth])->orderBy('id', 'desc')->first();
           
            if (count($invoice) > 0) {
                foreach ($invoice->payments as $payment) {
                   $paid += (int)$payment->amount_paid;
                }
            }
        }
        return [
            'total' => $total,
            'paid' => $paid,
            'to_collect' => $total - $paid,
        ];
    }

    public function getTotalPayable(Tenant $tenant)
    {

        $payment_term = $tenant->payment_term ? $tenant->payment_term: 'N/A';

        $floor = $tenant->unit->floor->level;

        $common_area = $this->getCommonArea($tenant);
        $common_area_charge = $this->getCommonAreaCharge($tenant);
        $interest = $this->getInterest($tenant, $common_area_charge);

        $rent = $this->getFloorRent($tenant);

        $service_charge = $this->getServiceCharge($tenant);
        $parking_fees = $this->getParkingCharges($tenant);
        $electricity = $this->getElectricityInfo($tenant);
        return $amount_payable = ($rent + $service_charge + $common_area_charge + $interest + $parking_fees + $electricity['bill']) * 1;
    }
}