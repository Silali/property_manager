<?php

namespace App\Traits;

use App\Floor;

trait HandlesFloorSpace
{
    public function setDesignatedArea(Floor $floor, $area)
    {
        $floor->update(['designated_area' => $area]);
    }

    public function setRentedArea(Floor $floor, $area)
    {
        $floor->update(['rented_area' => $area]);
    }
}