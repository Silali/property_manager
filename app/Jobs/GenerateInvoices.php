<?php

namespace App\Jobs;

use App\Mail\SendInvoice;
use App\Tenant;
use App\Traits\HandlesInvoices;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GenerateInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HandlesInvoices;

    public $period;

    /**
     * GenerateInvoices constructor.
     * @param $period
     * months  after which an invoice is generated
     */
    public function __construct($period)
    {
        $this->period = $period;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tenant = new Tenant();
        $query = $tenant->newQuery();

        if ($this->period == 1) {
            $query->whereHas('payment_term', function($term){
                $term->where('slug', 'monthly');
            });
        }

        elseif ($this->period == 3) {
            $query->whereHas('payment_term', function($term){
                $term->where('slug', 'quarterly');
            });
        } elseif ($this->period == 6) {
            $query->whereHas('payment_term', function($term){
                $term->where('slug', 'biannual');
            });
        }

        $tenants = $query->with('payment_term')->get();
        \Log::alert(json_encode($tenants));
        foreach ($tenants as $tenant) {
            $invoice = $this->createInvoice($tenant);
            \Mail::to($tenant->email)->send(new SendInvoice($invoice));
        }
    }
}
