<?php

namespace App\Http\Controllers;

use App\Tenant;
use App\TenantDocument;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Plank\Mediable\Media;
use Plank\Mediable\MediaUploaderFacade as MediaUploader;

class TenantDocumentController extends Controller
{
    public function store()
    {
        TenantDocument::truncate();
        $documents = array(
            ['name' => 'ID Document', 'description' => 'Personal ID Documents', 'tag' => 'id_document'],
            ['name' => 'Certification of Incorporation/Registration', 'tag' => 'coi'],
            ['name' => 'Bank Statement', 'description' => 'Bank Statement for the last 12 months', 'tag' => 'bank_statement'],
            ['name' => 'Annual Report/Audited Accounts', 'tag' => 'audit_report'],
            ['name' => 'Memorandum of Association', 'tag' => 'moa'],
            ['name' => 'Character References', 'tag' => 'character_reference'],
            ['name' => 'Reference from Landlord','tag' => 'landlord_reference'],
            ['name' => 'Business Licence','tag' => 'business_licence'],
            ['name' => 'Business PIN','tag' => 'business_pin'],
        );

        foreach ($documents as $document) {
            TenantDocument::create($document);
        }

        Toastr::success("Document types created");

        return back();
    }

    public function upload(Request $request, $id, $tag)
    {
        $tenant =  Tenant::withTrashed()->where('id', $id)->first();

        $file = $request->file;
        $date_paid = Carbon::now();
        $media = MediaUploader::fromSource($file)
            ->useFilename(str_replace(' ', '_', strtolower($tenant->name))."_".$date_paid."_".uniqid())
            ->toDestination('public', 'tenant_documents')
            ->upload();

        if ($tenant->hasMedia($tag))
        {
            $old = $tenant->getMedia($tag)->first();
            $filepath = storage_path('app/public/tenant_documents/').$old->filename.".".$old->extension;
            $old->delete();
            if (file_exists($filepath)) {
                \File::delete($filepath);
            }
        }

        $tenant->attachMedia($media, $tag);
    }

    public function getDocument($id, $tag)
    {
        $tenant = Tenant::withTrashed()->where('id', $id)->first();
        $document = $tenant->getMedia($tag)->first();
        $disk = $document->disk;
        $directory = $document->directory;
        $filename = $document->filename;
        $extension = $document->extension;



        $url = $disk.'/'.$directory.'/'.$filename.'.'.$extension;
        return response()->download(storage_path("app/".$url));
    }
}
