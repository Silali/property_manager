<?php

namespace App\Http\Controllers;

use App\PaymentTerm;
use App\Tenant;
use Illuminate\Http\Request;

class PaymentTermController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'period' => 'required'
        ]);

        $slug = str_replace(' ', '_', strtolower($request->name));

        $setting = PaymentTerm::where('slug', $slug)->first();

        if ((bool) $setting) {
            return response()->json([
                'status' => 'success'
            ]);
        }

        PaymentTerm::create([
            'name' => ucwords($request->name),
            'period' => $request->period,
            'slug' => $slug
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function get()
    {
        return PaymentTerm::orderBy('period', 'desc')->get();
    }

    public function delete(Request $request)
    {
        $term = PaymentTerm::find($request->id);
        if ($term->delete()) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'period' => 'required'
        ]);

        $term = PaymentTerm::find($request->id);
        $term->period = $request->period;
        $term->name = $request->name;
        $term->save();
        return response()->json([
            'status' => 'success'
        ]);
    }
}
