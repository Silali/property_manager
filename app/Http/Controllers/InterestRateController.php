<?php

namespace App\Http\Controllers;

use App\InterestRate;
use Illuminate\Http\Request;

class InterestRateController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'rate' => 'required'
        ]);

        $data = [
          'rate' => $request->rate
        ];

        if ((bool) InterestRate::create($data)) {
            return response()->json([
               'status' => 'success'
            ]);
        }
    }

    public function get()
    {
        return response()->json(
            InterestRate::orderBy('rate')->get()
        );
    }

    public function delete()
    {

    }
}
