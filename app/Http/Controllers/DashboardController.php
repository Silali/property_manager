<?php

namespace App\Http\Controllers;

use App\Charts\PropertyOccupancyRate;
use App\Property;
use App\Tenant;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public $pageTitle;

    public function __construct()
    {
        $this->pageTitle = "Dashboard";
    }
    public function index()
    {
        $title = $this->pageTitle;
        $tenants = Tenant::all();
        $properties = Property::all();
        $chart = new PropertyOccupancyRate();
        $total_rentable_area = [];
        $total_rented_area = [];
        $labels = [];
        foreach ($properties as $property) {
            array_push($labels, (string) $property->name);
            array_push($total_rentable_area, $property->total_rentable_area);
            array_push($total_rented_area, ($property->total_rentable_area - $property->undesignated_area));
        }
        $chart->dataset('Total Designated Area', 'bar', $total_rentable_area)->color('#FF8F81');
        $chart->dataset('Rented Area', 'bar', $total_rented_area)->color('#59B6FF');
        $chart->labels($labels);
        return view('dashboard', get_defined_vars());
    }
}
