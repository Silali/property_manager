<?php

namespace App\Http\Controllers;

use App\Events\PaymentAdded;
use App\Invoice;
use Barryvdh\DomPDF\Facade as PDF;
use App\Payment;
use App\PaymentMethod;
use App\Traits\HandlesInvoices;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Maatwebsite\Excel\Facades\Excel;
use Plank\Mediable\MediaUploaderFacade as MediaUploader;

class PaymentController extends Controller
{
    use HandlesInvoices;

    public function index(Request $request)
    {
        $invoice = new Invoice();
        $query = $invoice->newQuery();

        if ($request->has('from') && $request->has('to')) {
            if (!empty($request->to) && !empty($request->from)) {
                $from = Carbon::parse($request->from)->format('Y-m-d H:i:s');;
                $to = Carbon::parse($request->to)->addSeconds(86399)->format('Y-m-d H:i:s');
                $query->whereBetween('created_at', [$from, $to]);
            }
        } else {
            $start_date = Carbon::now()->startOfDay();
            $end_date =Carbon::now()->endOfDay();
            $query->whereBetween('created_at', [$start_date, $end_date]);
        }

        $invoices = $query->orderBy('created_at', 'desc')->get();
        $title = "Invoices";
        $filter = session()->get('filter_params');

        if ($request->has('excel')) {
            if (count($invoices) > 0) {
                return $this->exportRecords($invoices, $title,'excel');
            } else {
                Toastr::info('no records to export');
                return redirect('payments');
            }
        }

        if ($request->has('pdf')) {
            if (count($invoices) > 0) {
                return $this->exportRecords($invoices, $title,'pdf');
            } else {
                Toastr::info('no records to export');
                return redirect('payments');
            }
        }
        return view('payments.index', get_defined_vars());
    }

    public function create($id)
    {
        $invoice = Invoice::find($id);
        if ($invoice->cleared) {
            Toastr::info("Invoice #$invoice->id cleared");
            return redirect('payments');
        }
        $payments = $invoice->payments()->orderBy('date_paid')->get();
        $payment_methods = PaymentMethod::all();
        $title = "payment for invoice #$invoice->id";
        return view('payments.create', get_defined_vars());
    }

    public function store(Request $request, $invoice_id)
    {
        $this->validate($request, [
           'date_paid' => 'required',
            'amount' => 'required',
            'payment_method' => 'required',
            'document_file' => 'required | mimes:jpeg,png,pdf'
        ]);

        $invoice = Invoice::find($invoice_id);
        $payment_method = PaymentMethod::find($request->payment_method);
        $data = [
            'invoice_id' => $invoice_id,
            'amount_paid' => $request->amount,
            'date_paid' => Carbon::parse($request->date_paid),
            'payment_method' => $payment_method->name
        ];

        $payment = $invoice->payments()->create($data);
        $latest = $invoice->tenant->statement()->orderBy('id', 'desc')->first();
        $balance = 0;
        if (count($latest) > 0) {
            $balance = -1 * ($payment->amount_paid - $latest->balance);
        }
        $statement = [
            'date' => $payment->created_at,
            'receipt_id' => $payment->id,
            'balance' => $balance,
            'debit' => $payment->amount_paid
        ];
        $invoice->tenant->statement()->create($statement);
        if ((bool) $payment) {
            $tenant = str_replace(' ', '_', strtolower($invoice->tenant->name));
            $date_paid = Carbon::parse($request->date_paid)->format('Y_m_d');
            $file = $request->file('document_file');
            $media = MediaUploader::fromSource($file)
                ->useFilename($tenant."_".$payment_method->slug."_".$date_paid."_".uniqid())
                ->toDestination('public', 'payments')
                ->upload();

            $payment->attachMedia($media, 'document');
            if ($this->clearInvoice($invoice, $invoice->payments()->sum('amount_paid'))) {
                Toastr::info("Invoice #$invoice->id cleared");
                event(new PaymentAdded($payment));
                return redirect('payments');
            }
            $message = "payment for invoice #$invoice_id made successfully";
            event(new PaymentAdded($payment));
            Toastr::success($message);
            return back();
        } else {
            $message = "error adding payment try again";
            Toastr::error($message);
            return back();
        }

    }

    public function exportRecords($records, $title, $format)
    {
        $filename = strtolower($title)."_".uniqid();
        $items = $this->transform($records);
        if ($format == 'excel') {
            return Excel::create($filename, function($excel) use ($items) {
                $excel->sheet("invoices", function($sheet) use ($items){
                    $sheet->cells('A1:Z1', function ($cells) {
                        $cells->setBackground('#9BD3FF');
                        $cells->setAlignment('center');
                        $cells->setFont([
                            'bold' => true
                        ]);
                    });
                    $sheet->setBorder('A1:Z1', 'medium');
                    $sheet->fromArray($items);
                });
            })->download('xlsx');
        } elseif($format == 'pdf') {
            $pdf = PDF::loadView('pdf.payments', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download($filename.".pdf");
        }
    }

    public function transform($records)
    {
        $items = [];

        foreach ($records as $record) {
            $item = array(
                'Tenant' => (string)$record->tenant ? $record->tenant->name : 'N/A',
                'Building' => (string)$record->property_name,
                'Floor' => (string)$record->floor_level,
                'Unit' => (string)$record->unit_number,
                'Amount Due' => (string) number_format($record->amount_payable),
                'Payment Term' => (string)$record->payment_term,
                'Rate (%)' => (string)$record->rate_of_contract != 'N/A'? : 'N/A',
                'Period' => (string)$record->period_of_contract,
                'Status' =>(string) $record->cleared ? 'paid' : 'unpaid'
            );

            array_push($items, $item);
        }

        return $items;
    }

    public function getDocument($id)
    {
        $payment = Payment::find($id);
        $path = $payment->document_url;
        return response()->download(storage_path("app/".$path));
    }
}
