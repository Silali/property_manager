<?php

namespace App\Http\Controllers;

use App\Charts\PropertyOccupancyRate;
use App\Property;
use App\Setting;
use App\Traits\PropertyRentProjections;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Maatwebsite\Excel\Facades\Excel;

class PropertyController extends Controller
{
    use PropertyRentProjections;
    public $pageTitle;

    public $service_charge_rate;

    public function __construct()
    {
        $this->pageTitle = "Properties";
        $charge = Setting::where('key', 'service_charge')->first();
        $this->service_charge_rate = (bool)  $charge ? $charge->value : 0 ;
    }

    public function index(Request $request)
    {
        $property = new Property();
        $relations = ['floors', 'units'];
        $query = $property->newQuery();
        $title = $this->pageTitle;
        $properties = $query->with(['floors'])->get();
        if ($request->has('excel')) {
            if (count($properties) > 0) {
                return $this->exportRecords($properties, $title, 'excel');
            } else {
                Toastr::info("no records to export");
                return redirect('properties');
            }
        }
        if ($request->has('pdf')) {
            if (count($properties) > 0) {
                return $this->exportRecords($properties, $title, 'pdf');
            } else {
                Toastr::info("no records to export");
                return redirect('properties');
            }
        }
        return view('property.index', get_defined_vars());
    }

    public function store(Request $request)
    {
        $request->validate([
           'name' => 'required',
           'location' => 'required'
        ]);

        $data =  [
          'user_id' => \Auth::id(),
          'name' => $request->name,
          'location' => $request->location,
        ];

        $property = Property::create($data);
        if ((bool) $property) {
            $message = "Property $property->name created";
            Toastr::success($message);
            return [
                'status' => 'success'
            ];
        } else {
            $message = "Error creating item";
            Toastr::error($message);
            return [
              'status' => 'error',
            ];
        }    
    }

    public function update(Request $request, Property $property)
    {
        $request->validate([
           'name' => 'required',
           'location' => 'required'
        ]);

        if ((bool) $record = $property->find($request->id)) {
            $data =  [
              'name' => $request->name,
              'location' => $request->location
            ];

        $updated = $record->update($data);

        if ((bool) $updated) {
            $message = "Item updated successfully";
            Toastr::success($message);
            return [
                'status' => 'success'
            ];
        } else {
            return [
              'status' => 'error'
            ];
        }    
        }   
    }

    public function delete(Request $request)
    {
        $record = Property::find($request->id);
        if ((bool) $record) {
          $deleted = $record->delete();
          if ($deleted) {
            $message = "Item deleted successfully";
            Toastr::success($message);

            return [
                'status' => 'success'
            ];
          }
        }
    }

    public function exportRecords($records, $title, $format)
    {
        $filename = strtolower($title)."_".uniqid();
        $items = $this->transform($records);
        if ($format == 'excel') {
            return Excel::create($filename, function($excel) use ($items) {
                $sheet_name = 'properties';
                $excel->sheet($sheet_name, function($sheet) use ($items){
                    $sheet->cells('A1:Z1', function ($cells) {
                        $cells->setBackground('#9BD3FF');
                        $cells->setAlignment('center');
                        $cells->setFont([
                            'bold' => true
                        ]);
                    });
                    $sheet->setBorder('A1:Z1', 'medium');
                    $sheet->fromArray($items);
                });
            })->download('xlsx');
        } elseif($format == 'pdf') {
            $pdf = PDF::loadView('pdf.properties', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download($filename.".pdf");
        }
    }

    public function transform($records)
    {
        $transformed = [];
        foreach ($records as $record) {
            $t = array(
                'Name' => $record->name,
                'Location' => $record->location,
                'Floors' => (string) number_format(count($record->floors)),
                'Total Rentable Area' => (string) number_format($record->total_rentable_area),
                'Price per sq m' => (string) number_format($record->price_per_sq_m),
                'Tenants' => (string) number_format(count($record->tenants)),
                'Units' => (string) number_format(count($record->units)),
                'Undesignated Area' => (string) number_format($record->undesignated_area)
            );

            array_push($transformed, $t);
        }
        return $transformed;
    }

    public function getChart($id)
    {
        if (! (bool) $property = Property::find($id)) {
            return response()->json([
                'status' => 'error'
            ]);
        }
        return response()->json($this->graph($property, $this->service_charge_rate));
    }
}
