<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportControler extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:report_emails'
        ]);

        DB::table('report_emails')->insert([
            'name' => $request->name,
            'email' => $request->email
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function get()
    {
        return response()->json([
            'data' => DB::table('report_emails')->get()
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        DB::table('report_emails')->where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function delete($id)
    {
        DB::table('report_emails')->where('id', $id)->delete();
        return response()->json([
            'status' => 'success'
        ]);
    }
}
