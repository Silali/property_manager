<?php

namespace App\Http\Controllers;

use App\Tenant;
use Illuminate\Http\Request;

class ElectricityBillController extends Controller
{
    public function getBillInfo($id)
    {
        $tenant = Tenant::find($id);
        return response()->json([
            'data' => $tenant->electricity_bill
        ]);
    }

    public function store(Request $request, $id)
    {
        $tenant = Tenant::find($id);
        $this->validate($request, [
            'current' => 'required',
            'rate' => 'required',
            'previous' => 'required',
        ]);
        $data = [
            'current_usage' => $request->current,
            'previous_usage' => $request->previous,
            'current_rate' => $request->rate
        ];
        if (count($tenant->electricity_bill) > 0) {
            $tenant->electricity_bill()->update($data);
            return response()->json([
                'status' => 'success'
            ]);
        }

        $tenant->electricity_bill()->create($data);
        return response()->json([
            'status' => 'success'
        ]);
    }
}
