<?php

namespace App\Http\Controllers;

use App\Events\UserAdded;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Kamaln7\Toastr\Facades\Toastr;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	$users = User::all();
        $title = "Users";
    	return view('users.index', get_defined_vars());
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
    	    'name' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);

    	$password = strtoupper(Str::random(6));

    	$data = [
    	    'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($password)
        ];

        $user = User::create($data);

        event(new UserAdded($user, $password));

        Toastr::success("User $user->name created.");
        Toastr::info("Email sent to $user->email.");

        return response()->json([
            'status' => 'success'
        ]);
    }

    public function deactivate($id)
    {
        $user = User::find($id);
        $user->active = false;
        $user->save();

        Toastr::success("$user->name deactivated");
        return [
            'status' => 'success'
        ];
    }

    public function activate($id)
    {
        $user = User::find($id);
        $user->active = true;
        $user->save();

        Toastr::success("$user->name deactivated");

        return [
            'status' => 'success'
        ];
    }
}
