<?php

namespace App\Http\Controllers;

use App\IdDocument;
use Illuminate\Http\Request;

class IdDocumentController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $slug = str_replace(' ', '_', strtolower($request->name));

        $setting = IdDocument::where('slug', $slug)->first();

        if ((bool) $setting) {
            return response()->json([
                'status' => 'success'
            ]);
        }

        IdDocument::create([
            'name' => ucwords($request->name),
            'slug' => $slug
        ]);
    }

    public function get()
    {
        return IdDocument::orderBy('created_at', 'desc')->get();
    }

    public function delete(Request $request)
    {
        $document = IdDocument::find($request->id);
        if ($document->forceDelete()) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

}
