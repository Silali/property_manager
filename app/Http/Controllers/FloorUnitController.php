<?php

namespace App\Http\Controllers;

use App\Floor;
use App\FloorUnit;
use App\Providers\ValidationProviders\SpaceAvailableValidator;
use App\Providers\ValidationProviders\UniqueUnitValidator;
use App\Traits\HandlesFloorSpace;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class FloorUnitController extends Controller
{
    use HandlesFloorSpace;

    public function index($floor_id)
    {
        $floor = Floor::find($floor_id);
        $property = $floor->property;

        $units = $floor->units;
        $title = "offices in $floor->level @ $property->name";
        return view('floor.units.index', get_defined_vars());
    }

    public function store(Request $request, $floor_id)
    {
        $floor = Floor::find($floor_id);

        if ((bool) $floor) {
            $this->validate($request, [
                'unit_number' => [
                    'required',
                    new UniqueUnitValidator($request->unit_number, $floor)
                ],
                'area' => [
                    'required',
                    new SpaceAvailableValidator($floor->undesignated_area, $request->area)
                ]
            ]);

            $data = [
                'unit_number' => $request->unit_number,
                'area' => $request->area
            ];

            if ((bool) $floor_unit = $floor->units()->create($data)) {
                $area = $floor->units->sum('area');
                $this->setDesignatedArea($floor, $area);
                $message = "office added $floor_unit->name to $floor->level";
                Toastr::success($message);
                return [
                    'status' => 'success',
                ];
            } else {
                $message = "Error adding office";
                Toastr::error($message);
                return [
                    'status' => 'success',
                ];
            }
        }
    }

    public function update(Request $request, $unit_id)
    {
        $unit = FloorUnit::find($unit_id);
        $floor = $unit->floor;
        $this->validate($request, [
            'unit_number' => [
                'required'
            ],
            'area' => [
                'required',
                new SpaceAvailableValidator($floor->undesignated_area + $unit->area, $request->area)
            ]
        ]);

        $data = [
            'unit_number' => $request->unit_number,
            'area' => $request->area
        ];

        if ((bool) $unit->update($data)) {
            $area = $floor->units->sum('area');
            $property = $floor->property;
            $this->setDesignatedArea($floor, $area);
            $message = "office added $unit->name to $floor->level floor of $property->name";
            Toastr::success($message);
            return [
                'status' => 'success',
            ];
        } else {
            $message = "Error adding office";
            Toastr::error($message);
            return [
                'status' => 'success',
            ];
        }
    }

    public function getUnits($floor_id, Request $request)
    {
        $unit_id = null;
        if ($request->has('unit_id')) {
            $unit_id = $request->unit_id;
        }
        $all = Floor::find($floor_id)->units()->get();
        $unoccupied = $all->filter(function ($value, $key) use ($unit_id){
                return $value->occupied == false || $value->id == $unit_id;
        });
        return response()->json($unoccupied->toArray());
    }
}
