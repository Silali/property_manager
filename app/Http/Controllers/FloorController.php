<?php

namespace App\Http\Controllers;

use App\Charts\PropertyOccupancyRate;
use App\Floor;
use App\FloorUnit;
use App\Property;
use App\Traits\FloorRentProjections;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class FloorController extends Controller
{
    use FloorRentProjections;

    public function index($property_id)
    {
        $property = Property::find($property_id);
        $floors = $property->floors()->with('tenants')->get();
        $title = "Floors in $property->name";

        return view('floor.index', get_defined_vars());
    }

    public function store(Request $request, $property_id)
    {
        $property =  Property::find($property_id);
        $request->validate([
            'common_area' => 'required',
            'rentable_area' => 'required',
            'level' => 'required'
        ]);

        $data = [
            'level' => $request->level,
            'common_area' => $request->common_area,
            'total_rentable_area' => $request->rentable_area,
        ];

       $floor =  $property->floors()->create($data);

       if((bool) $floor) {
           Toastr::success("Floor created");
           return response()->json([
               'status' => 'success'
           ]);
       } else {
           Toastr::success("Error creating item");
           return response()->json([
               'status' => 'success'
           ]);
       }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
           'common_area' => 'required',
           'rentable_area' => 'required',
        ]);

        $floor = Floor::find($id);

        $data = [
            'level' => $request->level,
            'common_area' => $request->common_area,
            'total_rentable_area' => $request->rentable_area,
        ];

        $updated = $floor->update($data);

        if($updated) {
            Toastr::success("Item updated");
            return response()->json([
                'status' => 'success'
            ]);
        } else {
            Toastr::success("Error updating item");
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function delete(Request $request, $id)
    {
        $floor = Floor::find($id);
        if ($floor->delete()) {
            Toastr::success("Item deleted");
            return response()->json([
                'status' => 'success'
            ]);
        } else {
            Toastr::error("error deleting item");
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function getFloors($id)
    {
        return Property::find($id)->floors()->get();
    }

    public function getChart($id)
    {
        if (! (bool) $floor = Floor::find($id)) {
            return response()->json([
                'status' => 'error'
            ]);
        }
        return response()->json($this->graph($floor));
    }
}
