<?php

namespace App\Http\Controllers;

use App\Tenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class TenantStatementController extends Controller
{
    public function index(Request $request, $id)
    {
        $tenant = Tenant::where('id', $id)->withTrashed()->first();
        $filter = [];
        $statement = $tenant->statement();
        if ($request->has('from') && $request->has('to')) {
            if (!empty($request->to) && !empty($request->from)) {
                $from = Carbon::parse($request->from)->format('Y-m-d H:i:s');;
                $to = Carbon::parse($request->to)->addSeconds(86399)->format('Y-m-d H:i:s');
                $statement->whereBetween('date', [$from, $to]);
            }
        }

        $statement = $statement->get();

        if ($request->has('pdf')) {
            if (count($statement) > 0) {
                return $this->exportRecords($statement, "statement",'pdf');
            } else {
                Toastr::info('no records to export');
                return redirect('payments');
            }
        }
        return view('tenant.statement', get_defined_vars());
    }

    public function exportRecords($records, $title, $format)
    {
        $filename = strtolower($title)."_".uniqid();
        $items = $this->transform($records);
        if ($format == 'excel') {
            return Excel::create($filename, function($excel) use ($items) {
                $excel->sheet("invoices", function($sheet) use ($items){
                    $sheet->cells('A1:Z1', function ($cells) {
                        $cells->setBackground('#9BD3FF');
                        $cells->setAlignment('center');
                        $cells->setFont([
                            'bold' => true
                        ]);
                    });
                    $sheet->setBorder('A1:Z1', 'medium');
                    $sheet->fromArray($items);
                });
            })->download('xlsx');
        } elseif($format == 'pdf') {
            $pdf = PDF::loadView('pdf.statements', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download($filename.".pdf");
        }
    }

    public function transform($records)
    {
        $items = [];

        foreach ($records as $record) {
            $item = array(
                'is_receipt' => $record->invoice_id == null ? true : false,
                'Invoice/Receipt No' => $record->invoice_id == null ? "receipt #$record->receipt_id" : "invoice #$record->invoice_id",
                'Date' => $record->date,
                'Credit' => (string) number_format($record->credit),
                'Debit' => (string) number_format($record->debit),
                'Balance' => (string) number_format($record->balance),
            );

            array_push($items, $item);
        }

        return $items;
    }
}
