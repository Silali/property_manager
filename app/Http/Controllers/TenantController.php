<?php

namespace App\Http\Controllers;

use App\FloorUnit;
use App\IdDocument;
use App\InterestRate;
use App\Invoice;
use App\ParkingSpot;
use App\Payment;
use App\PaymentTerm;
use App\Property;
use App\Receipt;
use App\Tenant;
use App\TenantDocument;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use Plank\Mediable\MediaUploaderFacade as MediaUploader;

class TenantController extends Controller
{
    public function index(Request $request)
    {
        $tenant = new Tenant();
        $relations = ['parking_spots', 'payment_term', 'unit', 'floor', 'property', 'rate_of_contract', 'electricity_bill'];
        $properties = Property::all(['id', 'name']);
        $rates = InterestRate::all();
        $terms = PaymentTerm::all();
        $query = $tenant->newQuery();

        if ($request->has('property')) {
            if ($request->property != 'all') {
                $query->where('property_id', $request->property);
            }
        }

        if ($request->has('term')) {
            if ($request->term != 'all') {
                $query->where('payment_term_id', $request->term);
            }
        }

        if ($request->has('rate')) {
            if ($request->rate != 'all') {
                $query->where('rate_of_contract_id', $request->rate);
            }
        }

        $tenants = $query->with($relations)->orderBy('created_at', 'desc')->withTrashed()->get();
        $title = "Tenants";
        $filter = session()->get('filter_params');
        if ($request->has('excel')) {
            if (count($tenants) > 0) {
                return $this->exportRecords($tenants, $title, 'excel');
            } else {
                Toastr::info("no records to export");
                return redirect('tenants');
            }
        }

        if ($request->has('pdf')) {
            if (count($tenants) > 0) {
                return $this->exportRecords($tenants, $title, 'pdf');
            } else {
                Toastr::info("no records to export");
                return redirect('tenants');
            }
        }
        return view('tenant.index', get_defined_vars());
    }

    public function create()
    {
        $id_documents = IdDocument::select(['id', 'name'])->get();
        $payment_terms = PaymentTerm::select(['id', 'name'])->get();
        $interest_rates = InterestRate::all();
        $properties = Property::select(['id', 'name'])->get();
        $parking_spots = ParkingSpot::all();
        $spots = $parking_spots->filter(function ($value, $key) {
            return $value->occupied == false;
        });
        return view('tenant.create', get_defined_vars());
    }

    public function delete($id)
    {
        $tenant = Tenant::find($id);
        $unit = $tenant->unit;
        $unit->tenant_id = null;
        $unit->save();
        $tenant->delete();
        Toastr::success("$tenant->name tenancy terminated successfully");
        return [
            'status' => 'success'
        ];
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'nature_of_business' => 'required',
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'property' => 'required',
            'floor' => 'required',
            'id_document' => 'required',
            'id_number' => 'required',
            'payment_term' => 'required',
            'period_of_contract' => 'required',
            'rate_of_contract' => 'required',
            'unit' => 'required',
            'rent' => 'required',
            'common_area_rent' => 'required',
            'start_of_lease' => 'required',
            'operating_hours' => 'required',
            'weekend_operating_hours' => 'required',
            'parking_spot_rent' => 'required',
        ]);

        $unit = FloorUnit::find($request->unit);
        $document = IdDocument::find($request->id_document);

        $data = [
          'company_name' => $request->company_name,
          'name' => $request->name,
          'email' => $request->email,
          'phone' => str_replace('-', '', $request->phone),
          'property_id' => $request->property,
          'floor_id' => $request->floor,
          'unit_id' => $request->unit,
          'id_document' => $document->name,
          'id_number' => $request->id_number,
          'payment_term_id' => $request->payment_term,
          'rate_of_contract_id' => $request->rate_of_contract,
          'period_of_contract' => $request->period_of_contract,
          'start_of_lease' => Carbon::parse($request->start_of_lease),
          'rent' => $request->rent,
          'ca_rent' => $request->common_area_rent,
          'nature_of_business' => $request->nature_of_business,
          'parking_spot_rent' => $request->parking_spot_rent,
          'operating_hours' => $request->operating_hours,
          'weekend_operating_hours' => $request->weekend_operating_hours
        ];

        $tenant = Tenant::create($data);
        if ((bool) $tenant) {
            $unit->tenant_id = $tenant->id;
            $unit->save();
            if ($request->has('parking_spots')) {
                $parking_spots = ParkingSpot::whereIn('id', $request->parking_spots)->get();
                if (count($parking_spots) > 0) {
                    foreach ($parking_spots as $parking_spot) {
                        $parking_spot->tenant_id = $tenant->id;
                        $parking_spot->save();
                    }
                }
            }

            $message = "Tenant $tenant->name created";
            Toastr::success($message);
        } else {
            $message = "error creating item";
            Toastr::error($message);
        }

        return redirect('tenants');
    }

    public function edit($id)
    {
        $tenant = Tenant::withTrashed()->where('id', $id)->first();

        if (!(bool) $tenant) {
            Toastr::error('Could not find tenant');
            return redirect('tenants');
        }
        $id_documents = IdDocument::all();
        $payment_terms = PaymentTerm::select(['id', 'name'])->get();
        $interest_rates = InterestRate::all();
        $properties = Property::select(['id', 'name'])->get();
        $parking_spots = ParkingSpot::all();
        $spots = $parking_spots->filter(function ($value, $key) {
            return $value->occupied == false;
        });
        return view('tenant.edit', get_defined_vars());
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'company_name' => 'required',
            'nature_of_business' => 'required',
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'property' => 'required',
            'floor' => 'required',
            'id_document' => 'required',
            'id_number' => 'required',
            'payment_term' => 'required',
            'period_of_contract' => 'required',
            'rate_of_contract' => 'required',
            'unit' => 'required',
            'rent' => 'required',
            'common_area_rent' => 'required',
            'start_of_lease' => 'required',
            'operating_hours' => 'required',
            'weekend_operating_hours' => 'required',
            'parking_spot_rent' => 'required',
        ]);

        $unit = FloorUnit::find($request->unit);
        $document = IdDocument::find($request->id_document);

        $data = [
            'company_name' => $request->company_name,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => str_replace('-', '', $request->phone),
            'property_id' => $request->property,
            'floor_id' => $request->floor,
            'unit_id' => $request->unit,
            'id_document' => $document->name,
            'id_number' => $request->id_number,
            'payment_term_id' => $request->payment_term,
            'rate_of_contract_id' => $request->rate_of_contract,
            'period_of_contract' => $request->period_of_contract,
            'start_of_lease' => Carbon::parse($request->start_of_lease),
            'rent' => $request->rent,
            'ca_rent' => $request->common_area_rent,
            'nature_of_business' => $request->nature_of_business,
            'parking_spot_rent' => $request->parking_spot_rent,
            'operating_hours' => $request->operating_hours,
            'weekend_operating_hours' => $request->weekend_operating_hours
        ];

        $tenant = Tenant::find($id);
        if ($tenant->update($data)) {
            $unit->tenant_id = $request->unit;
            $unit->save();
            $message = "Tenant $tenant->name updated";
            if ($request->has('parking_spots')) {
                $parking_spots = ParkingSpot::whereIn('id', $request->parking_spots)->get();
                if (count($parking_spots) > 0) {
                    foreach ($parking_spots as $parking_spot) {
                        $parking_spot->tenant_id = $tenant->id;
                        $parking_spot->save();
                    }
                }
            }
            Toastr::success($message);
        }

        return redirect('tenants');
    }

    public function exportRecords($records, $title, $format)
    {
        $filename = strtolower($title)."_".uniqid();
        $items = $this->transform($records);
        if ($format == 'excel') {
            $properties = [];
            foreach ($records as $record) {
                $property = Property::find($record->property_id);
                array_push($properties, $property);
            }
            return Excel::create($filename, function ($excel) use ($items, $properties) {

                foreach ($properties as $property) {
                    $sheet_name = ucfirst(strtolower((bool) $property ? $property->name : 'Property Name Unavailable'));
                    $excel->sheet($sheet_name, function ($sheet) use ($items) {
                        $sheet->cells('A1:Z1', function ($cells) {
                            $cells->setBackground('#9BD3FF');
                            $cells->setAlignment('center');
                            $cells->setFont([
                                'bold' => true
                            ]);
                        });
                        $sheet->setBorder('A1:Z1', 'medium');
                        $sheet->fromArray($items);
                    });
                }
            })->download('xlsx');
        } elseif ($format == 'pdf') {
            $pdf = PDF::loadView('pdf.tenants', get_defined_vars());
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download($filename.".pdf");
        }
    }

    public function transform($records)
    {
        $transformed = [];

        foreach ($records as $record) {
            $item = array(
                'Name' => $record->name,
                'Email' => $record->email,
                'Phone' => $record->phone,
                'Property' => (bool)$record->property ? $record->property->name : 'N/A',
                'Floor' => (bool)$record->floor? $record->floor->level : 'N/A',
                'Rent per sq m' => $record->rent."Ksh",
                'Common Area Rent' => $record->ca_rent,
                'Unit' => (bool) $record->unit ? $record->unit->unit_number : 'N/A',
                'ID Document' => $record->id_document,
                'Payment Term' => (bool) $record->payment_term ? $record->payment_term->name : 'N/A',
                'Rate of Contract' => (bool)$record->rate_of_contract ? $record->rate_of_contract->rate."%" : 'N/A',
                'Period of Contract' => $record->period_of_contract. " months",
            );

            array_push($transformed, $item);
        }
        return  $transformed;
    }

    public function getDocument($id)
    {
        $tenant = Tenant::find($id);
        $path = $tenant->document_url;
        return response()->download(storage_path("app/".$path));
    }

    public function downloadInvoice($id)
    {
        $invoice = Invoice::find($id);
        if (count($invoice) > 0) {
            $filename = 'invoice_#'.$invoice->id;
            $pdf = PDF::loadView('pdf.invoice', get_defined_vars());
            $pdf->setPaper('A4', 'portrait');
            return $pdf->download($filename.".pdf");
        }
    }

    public function downloadReceipt($id)
    {
        $payment = Payment::find($id);
        $invoice = $payment->invoice;
        if (count($payment) > 0) {
            $filename = 'receipt_#'.$payment->id;
            $pdf = PDF::loadView('pdf.receipt', get_defined_vars());
            $pdf->setPaper('A4', 'portrait');
            return $pdf->download($filename.".pdf");
        }
    }

    public function uploadDocument(Request $request, $document)
    {
        $tenant = Tenant::where('id',$request->tenant)->withTrashed()->first();
        if ($request->hasFile('document')) {
            $file = $request->file('document');
            $date_paid = Carbon::now();
            $media = MediaUploader::fromSource($file)
                ->useFilename($tenant->name."_".$date_paid."_".uniqid())
                ->toDestination('public', 'payments')
                ->upload();

            $tenant->attachMedia($media, 'document');
        }
    }

    public function showDocuments($id)
    {
        $fix = false;
        $tenant = Tenant::where('id', $id)->with('media')->withTrashed()->first();
        $documents = TenantDocument::all();
        if (count($documents) != 9) {
            $fix = true;
        }
        return view('tenant.documents', get_defined_vars());
    }
}
