<?php

namespace App\Http\Controllers;

use App\Floor;
use App\ParkingSpot;
use App\Property;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class ParkingSpotController extends Controller
{
    public function index($floor_id)
    {
        $floor = Floor::find($floor_id);
        $property = $floor->property;

        $units = $floor->parking_spots;
        $title = "Parking spots in $floor->level @ $property->name";
        return view('floor.parking_spots.index', get_defined_vars());
    }

    public function store(Request $request, $floor_id)
    {
        $floor = Floor::find($floor_id);
        $request->validate([
            'spot_number' => 'required'
        ]);
        if ((bool) $floor) {
            $data = [
                'number' => $request->spot_number,
            ];

            if ((bool) $parking_spot = $floor->parking_spots()->create($data)) {
                $message = "spot added $parking_spot->number to $floor->level";
                Toastr::success($message);
                return [
                    'status' => 'success',
                ];
            } else {
                $message = "Error adding office";
                Toastr::error($message);
                return [
                    'status' => 'success',
                ];
            }
        }
    }

    public function update(Request $request, $unit_id)
    {
        $unit = ParkingSpot::find($unit_id);
        $this->validate($request, [
            'spot_number' => 'required'
        ]);

        $data = [
            'number' => $request->spot_number,
        ];

        if ((bool) $unit->update($data)) {
            $message = "spot updated";
            Toastr::success($message);
            return [
                'status' => 'success',
            ];
        } else {
            $message = "Error adding office";
            Toastr::error($message);
            return [
                'status' => 'success',
            ];
        }
    }

    public function delete()
    {

    }

    public function getUnits($floor_id, Request $request)
    {
        $unit_id = null;
        if ($request->has('unit_id')) {
            $unit_id = $request->unit_id;
        }
        $all = Floor::find($floor_id)->parking_spots()->get();
        $unoccupied = $all->filter(function ($value, $key) use ($unit_id){
            return $value->occupied == false || $value->id == $unit_id;
        });
        return $unoccupied;
    }

    public function getSpots($property_id)
    {
        $property = Property::find($property_id);
        return $property->parking_spots;
    }
}
