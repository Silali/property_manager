<?php

namespace App\Http\Controllers;

use App\IdDocument;
use App\PaymentTerm;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $title = "Settings";
        $tab = session()->get('filter_params');
        return view('settings', get_defined_vars());
    }
}
