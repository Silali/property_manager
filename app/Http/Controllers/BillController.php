<?php

namespace App\Http\Controllers;

use App\Bill;
use App\BillSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Plank\Mediable\MediaUploaderFacade as MediaUploader;

class BillController extends Controller
{
    public function index(Request $request)
    {
        $bill = new Bill();
        $query = $bill->newQuery();

        $current_year = Carbon::now()->year;
        $current_month = Carbon::now()->month;

        $copy = clone $query;
        $latest = $copy->orderBy('created_at', 'asc')->limit(1)->first();
        $years = [2018];
        if ((bool) $latest) {
            $years = range(Carbon::parse($latest->created_at)->year, Carbon::now()->year);
        }
        $months = [];

        foreach (range(1,12) as $i) {
            $month = [
                'value' => $i,
                'name' => Carbon::createFromFormat('m', $i)->format('F')
            ];

            array_push($months, $month);
        }

        if ($request->has('year')) {
            $current_year = (int)$request->year;
            $query->whereRaw('YEAR(created_at) = ?',[$request->year]);
        }

        if ($request->has('month')) {
            $current_month = (int)$request->month;
            $query->whereRaw('MONTH(created_at) = ?',[$request->month]);
        }

        $title = "Bills";
        $bills = $query->get();
        $amount_to_be_paid = 0;
        $amount_paid = 0;
        foreach ($bills as $bill) {
            if (! $bill->cleared) {
                $amount_to_be_paid += $bill->estimated_cost;
            }
            $amount_paid += $bill->amount;
        }

        return view('bills.index', get_defined_vars());
    }

    public function getSettings()
    {
        return response()->json([
            'status' => 'success',
            'data' => BillSetting::orderBy('estimated_cost', 'desc')->get()
        ]);
    }

    public function clear(Request $request, $id)
    {
        $this->validate($request, [
            'amount' => 'required',
            'document' => 'required|file|mimes:jpeg,png,pdf'
        ]);

        $bill = Bill::find($id);

        if (count($bill) > 0) {
            $bill->cleared = true;
            $bill->amount = $request->amount;
            $bill->save();

            $file = $request->file('document');

            $media = MediaUploader::fromSource($file)
                ->useFilename($bill->name."_".$bill->created_at->format('Y_m_d'))
                ->toDestination('public', 'bill')
                ->upload();

            $bill->attachMedia($media, 'document');
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function storeSetting(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required|integer'
        ]);

        $data = [
            'name' => $request->name,
            'estimated_cost' => $request->cost
        ];

        if ((bool) $setting =  BillSetting::create($data)) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function show(Bill $bill)
    {
        //
    }

    public function edit(Bill $bill)
    {
        //
    }

    public function updateSetting(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'cost' => 'required|integer'
        ]);

        $bill = BillSetting::find($id);

        $data = [
            'name' => $request->name,
            'estimated_cost' => $request->cost
        ];

        if ($bill->update($data)) {
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

    public function deleteSetting($id)
    {
        $setting = BillSetting::find($id);
        $setting->delete();
        return response()->json([
            'status' =>'success'
        ]);
    }

}
