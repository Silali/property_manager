<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class ServiceChargeController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'value' => 'required'
        ]);

        $key = 'service_charge';

        $data = [
            'key' => $key,
            'value' => $request->value,
            'value_type' => 'number',
        ];
        $setting = Setting::where('key', $key)->first();
        if ((bool) $setting) {
            $setting->value = $data['value'];
            $setting->save();
            return [
                'status' => 'success'
            ];
        }

        Setting::create($data);

        return [
            'status' => 'success'
        ];
    }

    public function get()
    {
        $service_charge = Setting::where('key', 'service_charge')->first();
        if ($service_charge == null) {
            $data = [
                'key' => 'service_charge',
                'value' => 0,
                'value_type' => 'number',
            ];

            $service_charge = Setting::create($data);
        }
        return $service_charge->value;
    }
}
