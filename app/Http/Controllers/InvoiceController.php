<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Tenant;
use Illuminate\Http\Request;
use App\Traits\HandlesInvoices;
use Barryvdh\DomPDF\Facade as PDF;

class InvoiceController extends Controller
{
    use HandlesInvoices;

    public function index($id)
    {
        $tenant = Tenant::find($id);
        return view('tenant.invoice', get_defined_vars());
    }

    public function store($id)
    {
        $this->createInvoice($id);
    }

    public function show(Invoice $model, $id)
    {
        $invoice = $model->find($id);
        $payments = $invoice->payments()->orderBy('created_at', 'desc')->paginate(10);
        $title = "Payment on invoice #$invoice->id";
        $total_paid = $invoice->payments()->sum('amount_paid');
        return view('payments.payments', get_defined_vars());
    }

    public function download(Invoice $model, $id)
    {
        $invoice = $model->find($id);
        $filename = "invoice_$id";
        $title = "invoice #$id";
        $pdf = PDF::loadView('pdf.invoice', get_defined_vars());
        $pdf->setPaper('A4', 'portrait');
        return $pdf->download($filename.".pdf");
    }

    public function destroy(Invoice $invoice)
    {
        $this->deleteInvoice();
    }
}
