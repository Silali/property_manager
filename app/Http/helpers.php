<?php

if (! function_exists('get_asset')) {
    function get_asset($path) {
        return asset("assets/$path");
    }
}

if (! function_exists('set_active_class')) {
    function set_active_class($segment, $index = null) {
        if (is_null($index)){
            return Request::segment(1) == $segment ? 'active' : null;
        }
        return Request::segment($index) == $segment ? 'active' : null;
    }
}

if (! function_exists('ordinal')) {
    function ordinal($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }
}

if (! function_exists('set_old')) {
    function set_old($key, $value) {
        $old = old($key);
        if (isset($old)) {
            return $old;
        }

        return $value;
    }
}

if (! function_exists('set_old_option')) {
    function set_old_option($key, $value, $current_option) {
        $old = old($key);
        if (isset($old)) {
            if ($old == $current_option) {
                return 'selected';
            }
        } elseif ($value == $current_option){
            return 'selected';
        }

        return '';
    }
}

if (! function_exists('set_current_param')) {
    function set_current_param(array $filter, $param, $option) {
        if (array_has($filter, $param)) {
            if ($filter[$param] == $option) {
                return 'selected';
            }
            return '';
        }
    }
}

if (! function_exists('set_filter_value')) {
    function set_filter_value(array  $filter, $param) {
        if (array_has($filter, $param)) {
            return $filter[$param];
        }
    }
}

if (! function_exists('set_tab')) {
    function set_tab(array $request, $key, $default = false) {
        if (array_has($request, 'tab')) {
            if ($request['tab'] == $key) {
                return 'active';
            }
            return '';
        }

        if ($default) {
            return 'active';
        }

    }
}

if (! function_exists('document_url')) {
    function document_url($document) {
        $disk = $document->disk;
        $directory = $document->directory;
        $filename = $document->filename;
        $extension = $document->extension;



        return $url = $disk.'/'.$directory.'/'.$filename.'.'.$extension;
    }
}