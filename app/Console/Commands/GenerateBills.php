<?php

namespace App\Console\Commands;

use App\Bill;
use App\BillSetting;
use Illuminate\Console\Command;

class GenerateBills extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:bills';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates Monthly Bills';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $settings = BillSetting::all();
        foreach ($settings as $setting) {
            $data = [
                'name' => $setting->name,
                'estimated_cost' => $setting->estimated_cost
            ];
            Bill::create($data);
        }
    }
}
