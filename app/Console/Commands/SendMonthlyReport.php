<?php

namespace App\Console\Commands;

use App\Mail\MonthlyReport;
use App\Traits\HandlesReport;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SendMonthlyReport extends Command
{
    use HandlesReport;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send\'s a monthly report';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = [];
        $data['offices'] = $this->getOfficesInfo();
        $data['parking'] = $this->getParkingSpotsInfo();
        $data['rent'] = $this->rent();
        $data['generated_on'] = Carbon::now('Africa/Nairobi')->format('M d, Y');
        $users = DB::table('report_emails')->pluck('email');
        Mail::to($users)->send(new MonthlyReport(json_decode(json_encode($data))));
    }
}
