<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:invoices {period}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates Invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \App\Jobs\GenerateInvoices::dispatch($this->argument('period'));
    }
}
