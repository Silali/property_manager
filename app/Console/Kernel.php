<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('inspire')
                  ->hourly();
//         Every Month
        $schedule->command('generate:invoices 1')
            ->cron('0 0 5 * *');
//        Every Quarter
         $schedule->command('generate:invoices 3')
             ->cron('0 0 5 */3 *');
//         Every 6 Months
        $schedule->command('generate:invoices 6')
            ->cron('0 0 5 */6 *');

        $schedule->command('send:reports')->monthlyOn(28);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
