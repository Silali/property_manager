<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Plank\Mediable\Mediable;

class Tenant extends Model
{
    use SoftDeletes, Mediable;

//    protected $with = ['parking_spots', 'payment_term', 'unit', 'floor', 'property', 'rate_of_contract'];

    protected $appends = ['woh_start', 'woh_end', 'oh_start', 'oh_end', 'document_url'];

    protected $guarded = [];

    public function parking_spots()
    {
        return $this->hasMany(ParkingSpot::class);
    }

    public function id_document()
    {
        return $this->hasOne(IdDocument::class);
    }

    public function payment_term()
    {
        return $this->belongsTo(PaymentTerm::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function unit()
    {
        return $this->belongsTo(FloorUnit::class);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function rate_of_contract()
    {
        return $this->belongsTo(InterestRate::class);
    }

    public function getWohStartAttribute()
    {
        if ((bool) $this->weekend_operating_hours) {
            $str = str_replace(' ', '', $this->weekend_operating_hours);
            $time = explode('-', $str);
            return  $time[0];
        }

        return '';
    }

    public function getWohEndAttribute()
    {
        if ((bool) $this->weekend_operating_hours) {
            $str = str_replace(' ', '', $this->weekend_operating_hours);
            $time = explode('-', $str);
            return $time[1];
        }

        return '';
    }

    public function getOhStartAttribute()
    {
        if ((bool) $this->operating_hours) {
            $str = str_replace(' ', '', $this->operating_hours);
            $time = explode('-', $str);
            return  $time[0];
        }

        return '';
    }

    public function getOhEndAttribute()
    {
        if ((bool) $this->operating_hours) {
            $str = str_replace(' ', '', $this->operating_hours);
            $time = explode('-', $str);
            return $time[1];
        }

        return '';
    }

    public function getDocumentUrlAttribute()
    {
        if(count($this->media) > 0) {
            $urls = [];
            $appDomain = url('/');
            foreach ($this->media as $photo) {

                $disk = $photo->disk;
                $directory = $photo->directory;
                $filename = $photo->filename;
                $extension = $photo->extension;



                $url = $disk.'/'.$directory.'/'.$filename.'.'.$extension;
                array_push($urls, $url);
            }

            return $urls[0];
        }
    }

    public function electricity_bill()
    {
        return $this->hasOne(ElectricityBill::class);
    }

    public function statement()
    {
        return $this->hasMany(TenantStatement::class);
    }
}
