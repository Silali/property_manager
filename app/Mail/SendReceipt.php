<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReceipt extends Mailable
{
    use Queueable, SerializesModels;

    public $payment;
    public $invoice;

    public function __construct($payment, $invoice)
    {
        $this->invoice = $invoice;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Receipt #".$this->payment->id)->markdown('emails.receipt');
    }
}
