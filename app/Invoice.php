<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $with = ['tenant'];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function getDueOnAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function receipts()
    {
        return $this->hasMany(Receipt::class);
    }

    public function statement()
    {
        return $this->hasMany(TenantStatement::class);
    }
}
