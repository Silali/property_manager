<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingSpot extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $appends = ['occupied'];

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    public function getOccupiedAttribute()
    {
        if (!(bool) $this->tenant) {
            return false;
        }
        return true;
    }
}
