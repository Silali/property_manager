<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Plank\Mediable\Mediable;

class Bill extends Model
{
    use Mediable;

    protected $guarded = [];

    protected $appends = ['document_url'];

    public function getDocumentUrlAttribute()
    {
        $urls = [];
        $appDomain = url('/');
        foreach ($this->media as $photo) {

            $disk = $photo->disk;
            $directory = $photo->directory;
            $filename = $photo->filename;
            $extension = $photo->extension;



            $url = 'storage/app/'.$disk.'/'.$directory.'/'.$filename.'.'.$extension;

            array_push($urls, $url);
        }

        return $urls[0];
    }
}
