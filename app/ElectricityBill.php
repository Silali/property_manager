<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ElectricityBill extends Model
{
    protected $guarded = [];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }
}
