<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FloorUnit extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $appends = ['occupied'];

    public function floor()
    {
        return $this->belongsTo(Floor::class);
    }

    public function tenant()
    {
        return $this->hasOne(Tenant::class, 'unit_id', 'id');
    }

    public function getOccupiedAttribute()
    {
        if (!(bool) $this->tenant) {
            return false;
        }
        return true;
    }
}
