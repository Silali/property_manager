<?php

namespace App\Listeners;

use App\Events\PaymentAdded;
use App\Mail\SendReceipt;
use App\Property;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class GenerateReceipts
{

    public function __construct()
    {

    }

    public function handle(PaymentAdded $event)
    {
        $payment = $event->payment;
        $invoice = $payment->invoice;
        \Log::info($invoice->tenant->email);
        Mail::to($invoice->tenant->email)->send(new SendReceipt($payment, $invoice));
    }
}
