<?php

namespace App\Listeners;

use App\Events\TenantAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Traits\HandlesInvoices;

class GenerateInvoice
{

    use HandlesInvoices;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TenantAdded  $event
     * @return void
     */
    public function handle(TenantAdded $event)
    {
        $this->createInvoice($event->tenant);
    }
}
