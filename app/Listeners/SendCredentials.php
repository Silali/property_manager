<?php

namespace App\Listeners;

use App\Events\UserAdded;
use App\Mail\UserCredentials;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendCredentials
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserAdded  $event
     * @return void
     */
    public function handle(UserAdded $event)
    {
        \Log::alert($event->user->email);
        \Log::alert($event->password);
        Mail::to($event->user->email)->send(new UserCredentials($event->user, $event->password));
    }
}
