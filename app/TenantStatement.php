<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantStatement extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }
}
