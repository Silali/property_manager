<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantDocument extends Model
{
    protected $guarded = [];
}
