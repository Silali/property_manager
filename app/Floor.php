<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Floor extends Model
{
	use SoftDeletes;
	
    protected $guarded = [];

    protected $hidden = ['updated_at', 'deleted_at', 'created_at'];

    protected $appends = ['rentable_area', 'undesignated_area', 'available_units'];

    public function parking_spots()
    {
        return $this->hasMany(ParkingSpot::class);
    }

    public function property()
    {
    	return $this->belongsTo(Property::class);
    }

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }

    public function units()
    {
        return $this->hasMany(FloorUnit::class);
    }

    public function getRentableAreaAttribute()
    {
        return $this->total_rentable_area - $this->rented_area;
    }

    public function getUndesignatedAreaAttribute()
    {
        return $this->total_rentable_area - $this->designated_area;
    }

    public function getAvailableUnitsAttribute()
    {
        $all = $this->units()->get();

        $unoccupied = $all->filter(function ($value, $key){
            return $value->occupied == false;
        });

        return count($unoccupied);
    }
}
