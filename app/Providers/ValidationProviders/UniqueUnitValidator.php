<?php
/**
 * Created by PhpStorm.
 * User: silali
 * Date: 4/12/18
 * Time: 3:24 PM
 */

namespace App\Providers\ValidationProviders;


use App\Floor;
use Illuminate\Contracts\Validation\Rule;

class UniqueUnitValidator implements Rule
{
    public $unitNumber;
    public $floor;
    public function __construct($unitNumber, Floor $floor)
    {
        $this->unitNumber = $unitNumber;
        $this->floor = $floor;
    }

    public function passes($attribute, $value)
    {
        if ((bool) $this->floor->property->units()->where('unit_number', $this->unitNumber)->first()) {
            return false;
        }

        return true;
    }

    public function message()
    {
        return 'The unit number already exists in this property';
    }
}