<?php
/**
 * Created by PhpStorm.
 * User: silali
 * Date: 4/11/18
 * Time: 4:56 PM
 */

namespace App\Providers\ValidationProviders;


use Illuminate\Contracts\Validation\Rule;

class SpaceAvailableValidator implements Rule
{
    public $availableArea;
    public $requestedArea;
    public function __construct($availableArea, $requestedArea)
    {
        $this->availableArea = $availableArea;
        $this->requestedArea = $requestedArea;
    }

    public function passes($attribute, $value)
    {
       if ($this->availableArea < $this->requestedArea) {
           return false;
       }

       return true;
    }

    public function message()
    {
        return 'the requested space is more than what is available';
    }
}