<?php
/**
 * Created by PhpStorm.
 * User: silali
 * Date: 4/29/18
 * Time: 2:10 PM
 */

namespace App\Providers\ValidationProviders;


class OperatingHoursValidator
{
    public $opening;
    public $closing;
    public function __construct($opening, $closing)
    {
        $this->closing = $closing;
        $this->opening = $opening;
    }

    public function passes($attribute, $value)
    {
        if ($this->opening == null || $this->closing == null ) {
            return false;
        }

        if ($this->opening == '' || $this->closing == '' ) {
            return false;
        }

        return true;
    }

    public function message()
    {
        return 'operating hours are needed';
    }
}