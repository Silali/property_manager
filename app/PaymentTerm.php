<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentTerm extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function tenants()
    {
        return $this->hasMany(Tenant::class);
    }
}
