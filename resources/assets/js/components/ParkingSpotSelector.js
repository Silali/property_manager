const ParkingSpotSelector = {
    props: ['getUrl', 'currentSpots'],
    created() {
        EventBus.$on('property-selected',(event) => {
            this.getSpots(event);
        });
    },
    data() {
        return {
            loading: false,
            spot: [],
            spotObject: [],
            spotOptions: []
        }
    },
    methods: {
        getSpots(event) {
            const propId = event.property;
            if  (propId === "" || propId == null) {
                return null;
            }

            axios.get(this.getUrl + '/' + propId)
                .then(response => {
                    this.spotOptions = [];
                    this.spotOptions = response.data;
                    var currentSpots = this.currentSpots;
                    var spotsArray = [];
                    for(var key in currentSpots) {
                        spotsArray.push(currentSpots[key].id);
                    }
                    console.log(spotsArray);
                    $('#parking_spots').select2('destroy');
                    $.each(spotsArray, function (i, spotId) {
                        $('#parking_spots').css({
                            'background': 'red'
                        });
                        $("#parking_spots option").each(function(){
                            $(this).prop('selected', true);
                        })
                    });
                    $('#parking_spots').select2();
                });
        },
    }
};

export default ParkingSpotSelector;