const modalOpener =  {
    props: [
        'updateItem',
        'modalName',
        'rowData'
    ],
    methods: {
        show: function () {
            if (this.updateItem == false) {
                this.$modal.show(this.modalName);
                return;
            }

            this.$modal.show(this.modalName, this.rowData)
        }
    }
};

export default modalOpener;