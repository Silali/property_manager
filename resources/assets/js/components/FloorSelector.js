const floorSelector =  {
    props: ['floorsUrl', 'propertyValue', 'floorValue', 'unitRentedValue', 'unitsUrl'],
    data () {
        return {
            property: '',
            propertySelected: false,
            floor: '',
            floorObject: null,
            floorOptions: '',
            loading: false,
            floorSelected: false,
            unit: '',
            unitOptions: [],
            unitObject: '',
            availableUnits: '',
        }
    },
    created() {
        if (this.propertyValue !== null || this.propertyValue !== "") {
            this.property = this.propertyValue;
        }
        if (this.areaRentedValue !== null || this.areaRentedValue !== "")  {
            this.areaRented = parseInt(this.areaRentedValue);
        }
        if (this.floorValue !== null || this.floorValue !== "") {
            this.getFloors();
            this.setFloor(this.floorValue);
        }
    },
    computed: {
        exceedsFloorArea() {
            return false;
        },
        rentableArea() {
            return this.floorObject.rentable_area;
        }
    },
    methods: {
        handleFloorSelection() {
            this.floorSelected = this.floorObject != null && this.floorObject.hasOwnProperty('total_rentable_area');
            if (this.floorSelected) {
                this.floor = this.floorObject.id;
                this.availableUnits = this.floorObject.available_units;
                this.getUnits(this.floor);
            }
        },
        getFloors() {
            const propId = this.property;
            EventBus.$emit('property-selected', {
                'property': propId
            });
            if (propId === '') {
                return;
            }
            let request = axios.get(this.floorsUrl+ '/' + propId);
            this.loading = true;
            request.then((response) => {
                this.floorOptions = response.data;
                this.loading = false;
            });
        },
        getUnits(floorId) {
            if (floorId === '') {
                return;
            }
            var context = this;
            let request = axios.get(this.unitsUrl+ '/' + floorId);
            this.loading = true;
            request.then((response) => {
                console.log(response.data);
                context.unitOptions = response.data;
                context.loading = false;
            });

        },
        handleUnitSelection() {
            this.unit = this.unitObject.id;
        },
        setFloor(value) {
            let floorId = this.property;
            if (floorId === '') {
                return;
            }
            let request = axios.get(this.floorsUrl+ '/' + this.property);
            this.loading = true;
            request.then((response) => {
                this.floorOptions = response.data;
                let floor = this.floorOptions.filter(function(obj) { return obj.id === parseInt(value) });
                this.floorObject = floor[0];
                this.floor = parseInt(value);
                this.loading = false;
                this.setUnits(this.floor);
            });
        },

        setUnits(floorId) {
            if (floorId === '') {
                return;
            }
            var context = this;
            let request = axios.get(this.unitsUrl+ '/' + floorId, {
                params: {
                    'unit_id': this.unitRentedValue
                }
            });
            this.loading = true;
            request.then((response) => {
                console.log(response.data);
                var units = response.data;
                for (var key in units) {
                    this.unitOptions.push(units[key]);
                }
                context.loading = false;
                let unit = this.unitOptions.filter(function(obj) { return obj.id === parseInt(context.unitRentedValue) });
                this.unitObject = unit[0];
                this.unit = parseInt(context.unitRentedValue);
                this.loading = false;
            });
        }
    }
};

export default floorSelector;