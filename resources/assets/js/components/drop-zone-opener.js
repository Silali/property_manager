const dropZoneOpener = {
    props: ['documentTag'],
    data() {
        return {

        }
    },
    methods: {
        openUploader() {
            EventBus.$emit('open-uploader', {
                'tag': this.documentTag
            });
        }
    }
};

export default dropZoneOpener;