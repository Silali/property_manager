import VModal from 'vue-js-modal';
import floorSelector from  './components/FloorSelector';
import modalOpener from './components/ModalOpener';
import dropZoneOpener from './components/drop-zone-opener';
import 'vue2-dropzone/dist/vue2Dropzone.min.css';
import Toasted from 'vue-toasted';
import ParkingSpotSelector from "./components/ParkingSpotSelector";

require('./bootstrap');

window.Vue = require('vue');
Vue.use(VModal);
Vue.use(Toasted);

window.EventBus = new Vue();

Vue.component('property-editor', require('./components/PropertyEditor.vue'));
Vue.component('property-projection-chart', require('./components/PropertyProjectionChart'));
Vue.component('payment-term-modal', require('./components/PaymentTermModal'));
Vue.component('floor-projection-chart', require('./components/FloorProjectionChart'));
Vue.component('floor-editor', require('./components/FloorEditor'));
Vue.component('parking-spot-editor', require('./components/ParkingSpotEditor'));
Vue.component('interest-rate-setter', require('./components/InterestRateSetter.vue'));
Vue.component('id-document-editor', require('./components/IdDocumentEditor'));
Vue.component('payment-term-editor', require('./components/PaymentTermEditor'));
Vue.component('floor-unit-editor', require('./components/FloorUnitEditor'));
Vue.component('user-editor', require('./components/UserEditor'));
Vue.component('service-charge-editor', require('./components/ServiceChargeEditor'));
Vue.component('floor-selector', floorSelector);
Vue.component('parking-selector', ParkingSpotSelector);
Vue.component('modal-opener', modalOpener);
Vue.component('dropzone-opener', dropZoneOpener);
Vue.component('document-uploader', require('./components/DocumentUploader'));
Vue.component('elec-bill-editor', require('./components/ElecBillEditor'));
Vue.component('bills-setting-editor', require('./components/BillsSettingEditor'));
Vue.component('bill-clearance-editor', require('./components/BillClearanceEditor'));
Vue.component('report-emails-editor', require('./components/ReportEmailsEditor'));
Vue.component('delete-item', {
    props: {
        deleteUrl: {
            default: ''
        },
        message: {
            default: 'do you want to delete an item?'
        }
    },
    methods: {
        created() {
            if (this.message === undefined) {
                this.message = ''
            }
        },
        deleteItem() {
            if (confirm(this.message)) {
                var request = axios.post(this.deleteUrl);

                request.then((response) => {
                    if (response.data.status === 'success') {
                        window.location.reload(false);
                    }
                });
            }

            return null;
        }
    }
});
Vue.component('chart-opener', {
    props: ['chartName'],
    methods: {
        showChart() {
            this.$emit('show-chart', {
                chartName: this.chartName
            })
        }
    }
});

const app = new Vue({
    el: '#app'
});
