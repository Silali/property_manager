@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                    <modal-opener
                            modal-name="user-editor"
                            :row-data="{ name: '',email: ''}"
                            :update-item="false"
                            inline-template>
                        <button class="btn btn-sm btn-primary pull-right" @click="show">Add User</button>
                    </modal-opener>
                </div>
                <div class="card-body">
                    @if(count($users) > 0)
                        <table class="table table-condensed table-striped" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $user)
                                <tr>
                                    <th>{{ $key + 1 }}</th>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td align="right">
                                        @if($user->active)
                                            <delete-item
                                                    delete-url='{{ url('users/deactivate', $user->id) }}'
                                                    message="deactivate {{ $user->name }} ?"
                                                    inline-template>
                                                <button class="btn btn-xs btn-danger" @click="deleteItem">Deactivate</button>
                                            </delete-item>
                                        @else
                                            <delete-item
                                                    delete-url='{{ url('users/activate', $user->id) }}'
                                                    message="activated {{ $user->name }} ?"
                                                    inline-template>
                                                <button class="btn btn-xs btn-secondary" @click="deleteItem">Activate</button>
                                            </delete-item>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <user-editor
            create-url="{{ url('user/create') }}"
            update-url="{{ url('user/update') }}"
    ></user-editor>
@endsection