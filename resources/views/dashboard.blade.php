@extends("layouts.app")
@section('title')
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <a class="round bg-primary" href="javascript:void(0)">
                        <i class="mdi mdi-account text-white"></i>
                    </a>
                    <h2 class="m-t-10 m-b-0">{{ number_format(count($tenants)) }}</h2>
                    <h6>Tenants</h6>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <a class="round bg-primary" href="javascript:void(0)">
                        <i class="mdi-domain mdi text-white"></i>
                    </a>
                    <h2 class="m-t-10 m-b-0">{{ number_format(count($properties)) }}</h2>
                    <h6>Properties</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card m-t-15">{!! $chart->container() !!}</div>
        </div>
    </div>
@endsection
@section("chart")
    {!! $chart->script() !!}
@endsection