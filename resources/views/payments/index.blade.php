@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-5">
                <div class="card-body" style="padding: 0.5rem">
                    <form method="get" action="{{ url('payments') }}">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <div>
                                    <label class="mb-1"><small><strong>date generated</strong></small></label>
                                </div>
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control" name="from" placeholder="From Date" value="{{ set_filter_value($filter, 'from') }}">
                                    <input type="text" class="form-control" name="to" placeholder="To Date" value="{{ set_filter_value($filter, 'from') }}">
                                </div>
                            </div>
                            <div class="col-auto mt-4">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <button type="submit" name="excel" value="1" class="btn btn-success">Excel</button>
                                <button type="submit" name="pdf" value="1" class="btn btn-danger">PDF</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                </div>
                <div class="card-body">
                    @if(count($invoices) > 0)
                        <table class="table table-sm table-striped" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Tenant</th>
                                <th>Property</th>
                                <th>Amount Payable</th>
                                <th>Payment Term</th>
                                <th>Due On</th>
                                <th align="right" style="width:81.2px;"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $key => $invoice)
                                <tr>
                                    <th>{{ $key + 1 }}</th>
                                    <td>{{ (bool) $invoice->tenant ? $invoice->tenant->name : 'N?A' }}</td>
                                    <td>{{ $invoice->floor_level }} floor of {{ $invoice->property_name }} office: <strong>{{ $invoice->unit_number }}</strong></td>
                                    <td>{{ number_format($invoice->amount_payable) }}</td>
                                    <td>{{ $invoice->payment_term }}</td>
                                    <td>{{ $invoice->due_on->format('M d, Y') }}</td>
                                    <td align="right">
                                        <a href="{{ url('payments/invoices/view', $invoice->id) }}" class="btn btn-xs btn-primary" target="_blank">View</a>
                                        @if(! $invoice->cleared)
                                            <a href="{{ url('payments/invoices/clear/create', $invoice->id) }}" class="btn btn-xs btn-success" target="_blank">Clear</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script src="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
            jQuery('#date-range').datepicker({
                toggleActive: true
            });
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <property-editor
            create-url="{{ url('properties/create') }}"
            update-url="{{ url('properties/update') }}"
    ></property-editor>
@endsection