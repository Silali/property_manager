@extends("layouts.app")
@section("title")
    {{ $title }} <a href="#" onclick="event.preventDefault()"><strong><small>See Invoice</small></strong></a>
@endsection
@section("content")
    <div class="row">
        <div class="col-md-4">
            <form action="{{ url('payments/invoices/clear/store', $invoice->id) }}" method="post"  enctype="multipart/form-data">
            <div class="card m-t-30">
                <div class="card-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="date_paid">Date Paid</label>
                            <input type="text" id="date_paid" name="date_paid" class="form-control" autocomplete="off">
                            @if($errors->has('date_paid'))
                                <span class="text-danger"><small>{{ $errors->first('date_paid') }}</small></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="amount">Amount Paid <small>Ksh</small></label>
                            <input type="number" id="amount" name="amount" class="form-control">
                            @if($errors->has('amount'))
                                <span class="text-danger"><small>{{ $errors->first('amount') }}</small></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="payment_method">Payment Method</label>
                            <select name="payment_method" id="payment_method" class="form-control">
                                <option value="">Select a Method</option>
                                @foreach($payment_methods as $payment_method)
                                    <option value="{{ $payment_method->id }}">{{ $payment_method->name }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('payment_method'))
                                <span class="text-danger"><small>{{ $errors->first('payment_method') }}</small></span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="document_file">Upload Document</label>
                            <input type="file" id="document_file" name="document_file">
                            @if($errors->has('document_file'))
                                <span class="text-danger"><small>{{ $errors->first('document_file') }}</small></span>
                            @endif
                        </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-primary btn-block">Submit</button>
                </div>
            </div>
            </form>
        </div>
        <div class="col-md-8">
            <div class="card m-t-30 m-b-30">
                <div class="card">
                    <div class="card-header">
                        Payments
                    </div>
                    <div class="card-body">
                        @if(count($payments) > 0)
                            <table class="table-striped table-condensed table">
                                <thead>
                                    <tr>
                                        <th>Date Paid</th>
                                        <th>Payment Method</th>
                                        <th>Amount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td>{{ $payment->date_paid }}</td>
                                            <td>{{ $payment->payment_method }}</td>
                                            <td>{{ $payment->amount_paid }}</td>
                                            <td><a href="{{ url('payments/document', $payment->id) }}" target="_blank"><i class="mdi mdi-download"></i>  Document</a> </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Payments made yet
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section("modals")
    <div class="modal">
        <div class="card m-t-30 m-b-30">
            <div class="card-body invoice">
                <div class="clearfix">
                    <div class="pull-left">
                        <a href="index.html" class="logo"><i class="fa fa-home"></i> PM</a>
                    </div>
                    <div class="pull-right">
                        <h6>Invoice : #<strong>{{ $invoice->id }}</strong></h6>
                        <h6 class="pull-right">Date Generated: {{ $invoice->created_at->format('M d, Y') }}</h6>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-12">

                        <div class="pull-left mt-4">
                            <address>
                                <strong>Twitter, Inc.</strong><br>
                                795 Folsom Ave, Suite 600<br>
                                San Francisco, CA 94107<br>
                                (123) 456-7890
                            </address>
                        </div>
                        <div class="pull-right mt-4">
                            <p><strong>Payment Due on: </strong> March 15, 2015</p>
                            <p><strong>Order Status: </strong> <span class="badge badge-warning">Pending</span></p>
                            <p><strong>Order ID: </strong> #123456</p>
                        </div>
                    </div>
                </div><!--end row-->

                <div class="h-50"></div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table mt-4">
                                <thead>
                                <tr>
                                    <th>Property</th>
                                    <th>Floor Level</th>
                                    <th>Sq m Rented</th>
                                    <th>Unit Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>{{ $invoice->property_name }}</td>
                                    <td>{{ $invoice->floor }}</td>
                                    <td>{{ $invoice->area_rented }}</td>
                                    <td>{{ number_format(20000) }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!--end row-->

                <div class="row" style="border-radius: 0px;">
                    <div class="col-md-9">
                        <p><strong>Terms And Condition : </strong></p>
                        <ul>
                            <li><small>All accounts are to be paid within 7 days from receipt of invoice. </small></li>
                            <li><small>To be paid by cheque or credit card or direct payment online.</small></li>
                            <li><small> If account is not paid within 7 days the credits details supplied as confirmation<br> of work undertaken will be charged the agreed quoted fee noted above.</small></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <p class="text-right">Payment Term: <strong>{{ $invoice->payment_term }}</strong></p>
                        <p class="text-right">Rate: <strong>{{ $invoice->rate_of_contract }}%</strong></p>
                        <p class="text-right">VAT: 12.9%</p>
                        <hr>
                        <h4 class="text-right">$ 2930.00</h4>
                    </div>
                </div><!--end row-->
                <hr>
            </div>
        </div>
    </div>
@endsection
@section("js")
    <script src="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('#date_paid').datepicker({
                autoclose: true
            });
        });
    </script>
@endsection
@section("css")
    <link rel="stylesheet" href="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection