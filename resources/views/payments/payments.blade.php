@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
           <div class="row">
               <div class="col-md-12">
                   <div class="m-b-10 pull-right">
                       {{ $payments->links('vendor.pagination.bootstrap-4') }}
                   </div>
               </div>
           </div>
            <div class="row">
                <div class="col-12">
                    <div class="m-b-30">
                        <span class="pull-right"><strong>Total Amount Due:</strong> {{ number_format($invoice->amount_payable) }} Ksh</span><br>
                        <span class="pull-right"><strong>Total Paid:</strong> {{ number_format($total_paid) }} Ksh</span>
                    </div>
                </div>
            </div>
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left">{{ $title }}</h4>
                    <a href="{{ url('payments/invoices/download', $invoice->id) }}" target="_blank" class="btn btn- btn-outline-primary pull-right">download invoice</a>
                </div>
                <div class="card-body">
                    @if(count($payments) > 0)
                        <table class="table  table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Amount Paid (Ksh)</th>
                                <th>Method</th>
                                <th>Paid On</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $key => $payment)
                                <tr>
                                    <th>{{ $key + 1 }}</th>
                                    <td>{{  number_format($payment->amount_paid) }}</td>
                                    <td>{{ $payment->payment_method }}</td>
                                    <td>{{ $payment->date_paid }}</td>
                                    <td><a href="{{ url('payments/document', $payment->id) }}" target="_blank"><i class="mdi mdi-download"></i>  Document</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection