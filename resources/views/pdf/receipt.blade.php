@extends("layouts.pdf")
@section("content")
    <table class="table table-sm inner-body" align="center" width="570" cellpadding="0" cellspacing="0">
        <!-- Body content -->
        <tr>
            <td class="content-cell">
                # Receipt for invoice #{{ $invoice->id }}
                <p style="font-size: 12px"><strong>Generated On:</strong> {{ $payment->created_at->format('M d, Y H:i a') }}</p>

                Hello <strong>{{ ucwords(strtolower($invoice->tenant->name)) }}</strong>, Your payment has been received
                <hr>
                # Payment Details
                <hr>
                <div class="table">
                    <table class="table table-sm table-bordered">
                        <thead>
                        <tr>
                            <th align="left">Amount</th>
                            <th align="right" style="font-weight: normal">{{ number_format($payment->amount_paid) }} Ksh</th>
                        </tr>
                        <tr>
                            <th align="left">Paid On</th>
                            <th align="right" style="font-weight: normal">{{ \Carbon\Carbon::parse($payment->date_paid)->format('M d, Y') }}</th>
                        </tr>
                        <tr>
                            <th align="left">Payment Method</th>
                            <th align="right" style="font-weight: normal">{{ $payment->payment_method  }}</th>
                        </tr>
                        <tr>
                            <th align="left">Payment Ref ID</th>
                            <th align="right" style="font-weight: normal">{{ $payment->id  }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <p>Being Payment For</p>
                <div class="table">
                    <table class="table table-sm table-bordered">
                        <thead>
                        <tr>
                            <th align="left">Floor</th>
                            <th align="right" style="font-weight: normal">{{ $invoice->floor_level }}</th>
                        </tr>
                        <tr>
                            <th align="left">Building</th>
                            <th align="right" style="font-weight: normal">{{ $invoice->property_name }}</th>
                        </tr>
                        <tr>
                            <th align="left">Office</th>
                            <th align="right" style="font-weight: normal">{{ $invoice->unit_number }}</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                Thanks,<br>
                {{ config('app.name') }}
            </td>
        </tr>
    </table>
@endsection