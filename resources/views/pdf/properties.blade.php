@extends("layouts.pdf")
@section("content")
    <div class="row">
        <h1>{{ $title }}</h1>
    </div>
    <div class="row">
        <table class="table-sm  table-striped table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Location</th>
                <th>Floors</th>
                <th>Total Rentable Area</th>
                <th>Price per sq m</th>
                <th>Designated Offices</th>
                <th>Undesignated Area <small>sq m</small></th>
                <th>Tenants</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $key => $item)
                <tr>
                    <th>#</th>
                    <td>{{ $item['Name'] }}</td>
                    <td>{{ $item['Location'] }}</td>
                    <td>{{ $item['Floors'] }}</td>
                    <td>{{ $item['Total Rentable Area'] }}</td>
                    <td>{{ $item['Price per sq m'] }}</td>
                    <td>{{ $item['Units'] }}</td>
                    <td>{{ $item['Undesignated Area'] }}</td>
                    <td>{{ $item['Tenants'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection