@extends("layouts.pdf")
@section("content")
    <div class="row">
        <h1>{{ $title }}</h1>
    </div>
    <div class="row">
        <table class="table-sm  table-striped table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Property</th>
                <th>Floor</th>
                <th>Office</th>
                <th>ID Document</th>
                <th>Payment Term</th>
                <th>Rate of Contract</th>
                <th>Period of Contract</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $key => $item)
                <tr>
                    <th>{{ $key + 1 }}</th>
                    <td>{{ $item['Name']}}</td>
                    <td>{{ $item['Email'] }}</td>
                    <td>{{ $item['Phone'] }}</td>
                    <td>{{ $item['Property'] }}</td>
                    <td>{{ $item['Floor'] }}</td>
                    <td>{{ $item['Unit'] }}</td>
                    <td>{{ $item['ID Document'] }}</td>
                    <td>{{ $item['Payment Term'] }}</td>
                    <td>{{ $item['Rate of Contract'] }}</td>
                    <td>{{ $item['Period of Contract'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection