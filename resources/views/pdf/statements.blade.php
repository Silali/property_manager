@extends("layouts.pdf")
@section("content")
    @if(count($items) > 0)
        <table class="table table-sm" >
            <thead>
                <tr>
                    <th>#</th>
                    <th>Receipt/Invoice No</th>
                    <th>Date </th>
                    <th>Credit</th>
                    <th>Debit</th>
                    <th>Balance</th>
                </tr>
            </thead>
            <tbody>
            @foreach($items as $key => $invoice)
                <tr class="{{ $invoice['is_receipt'] ?: 'table-secondary' }}">
                    <td>{{ $key + 1 }}</td>
                    <td> {{ $invoice['Invoice/Receipt No'] }}</td>
                    <td>{{ $invoice['Date'] }}</td>
                    <td>{{ $invoice['Credit'] }}</td>
                    <td>{{ $invoice['Debit'] }}</td>
                    <td>{{ $invoice['Balance'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
    @endif
@endsection