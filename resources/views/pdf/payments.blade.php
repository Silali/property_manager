@extends("layouts.pdf")
@section("content")
    <div class="row">
        <h1>{{ $title }}</h1>
    </div>
    <div class="row">
        <table class="table-sm  table-striped table">
            <thead>
            <tr>
                <th>#</th>
                <th>Tenant</th>
                <th>Building</th>
                <th>Floor</th>
                <th>Office</th>
                <th>Amount Due <small>KSh</small></th>
                <th>Rate (%)</th>
                <th>Period</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $key => $item)
                <tr>
                    <th>{{ $key + 1 }}</th>
                    <td>{{ $item['Tenant']}}</td>
                    <td>{{ $item['Building'] }}</td>
                    <td>{{ $item['Floor'] }}</td>
                    <td>{{ $item['Unit'] }}</td>
                    <td>{{ $item['Amount Due'] }}</td>
                    <td>{{ $item['Rate (%)'] }}</td>
                    <td>{{ $item['Period'] }}</td>
                    <td>{{ $item['Status'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection