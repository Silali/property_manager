@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-sm-12">
            <div class="card m-b-10">
                <div class="card-body" style="padding:0.2rem;">
                    <ul class="nav nav-pills nav-justified" role="tablist">
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'interest_rate', true) }}" href="{{ url('settings?tab=') }}interest_rate" role="tab">Interest Rate</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'id_document') }}" href="{{ url('settings?tab=') }}id_document" role="tab">ID Documents</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'payment_terms') }}"  href="{{ url('settings?tab=') }}payment_terms" role="tab">Payment Terms</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'service_charge') }}"  href="{{ url('settings?tab=') }}service_charge" role="tab">Service Charge</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'report') }}"  href="{{ url('settings?tab=') }}report" role="tab">Report</a>
                        </li>
                        <li class="nav-item waves-effect waves-light">
                            <a class="nav-link {{ set_tab($tab, 'bills') }}"  href="{{ url('settings?tab=') }}bills" role="tab">Bills</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane {{ set_tab($tab, 'interest_rate', true) }}">
                            <interest-rate-setter
                                    save-url="{{ url('settings/interest_rate/set') }}"
                                    get-url="{{ url('settings/interest_rate/get') }}"
                                    delete-url="{{ url('settings/interest_rate/delete') }}">
                            </interest-rate-setter>
                        </div>
                        <div class="tab-pane {{ set_tab($tab, 'id_document') }}">
                            <id-document-editor
                                    get-url="{{ url('settings/id_documents/get') }}"
                                    save-url="{{ url('settings/id_documents/add') }}"
                                    delete-url="{{ url('settings/id_documents/delete') }}"></id-document-editor>
                        </div>
                        <div class="tab-pane {{ set_tab($tab, 'payment_terms') }}">
                            <payment-term-editor
                                    get-url="{{ url('settings/payment_terms/get') }}"
                                    save-url="{{ url('settings/payment_terms/add') }}"
                                    delete-url="{{ url('settings/payment_terms/delete') }}"></payment-term-editor>
                        </div>
                        <div class="tab-pane {{ set_tab($tab, 'service_charge') }}">
                            <service-charge-editor
                            save-url="{{ url('settings/service_charge') }}"
                            get-url="{{ url('settings/get_service_charge') }}"></service-charge-editor>
                        </div>
                        <div class="tab-pane {{ set_tab($tab, 'bills') }}">
                           <bills-setting-editor
                           edit-url="{{ url('settings/bills/update') }}"
                           save-url="{{ url('settings/bills/create') }}"
                           get-url="{{ url('settings/bills/get') }}"
                           delete-url="{{ url('settings/bills/delete') }}">

                           </bills-setting-editor>
                        </div>
                        <div class="tab-pane {{ set_tab($tab, 'report') }}">
                            <report-emails-editor
                                    edit-url="{{ url('settings/reports/update') }}"
                                    save-url="{{ url('settings/reports/create') }}"
                                    get-url="{{ url('settings/reports/get') }}"
                                    delete-url="{{ url('settings/reports/delete') }}">

                            </report-emails-editor>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("modals")
    <payment-term-modal
    post-url="{{ url('settings/payment_terms/update') }}"></payment-term-modal>
@endsection