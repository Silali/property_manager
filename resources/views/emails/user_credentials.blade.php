@component('mail::message')
<div class="logo" style="height: 70px;margin: 20px 0;">
    <hr>
    <img src="{{ get_asset("images/logo.png") }}" style="display: block; margin: 0 auto;" alt="trancewood logo">
    <hr>
</div>
# Hello {{ $user->name }}

You have been added to <strong>TranceWood Property Management System</strong> as a user.<br>
Your login credentials<br>
<strong>Email:</strong> {{ $user->email }} <br>
<strong>Password:</strong> {{ $password }} <br>
<hr>
@component('mail::button', ['url' => url('login')])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
