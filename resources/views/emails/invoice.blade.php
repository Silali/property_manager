@component('mail::message')
<div class="logo" style="height: 70px;margin: 20px 0;">
    <hr>
    <img src="{{ get_asset("images/logo.png") }}" style="display: block; margin: 0 auto;" alt="trancewood logo">
    <hr>
</div>
### Invoice to {{ $invoice->tenant ? $invoice->tenant->name : 'N/A' }}
<hr>
<p style="font-size: 12px"><strong></strong>{{ $invoice->property }}</p>
<p style="font-size: 12px"><strong>Floor No - </strong>{{ $invoice->floor_level }}</p>
<p style="font-size: 12px"><strong>Office - </strong>{{ $invoice->unit_number  }}</p>
<hr>
<p style="font-size: 12px"><strong>Balance Due -</strong> {{ number_format($invoice->amount_payable) }}</p>
<p style="font-size: 12px"><strong>Payment Term -</strong> {{ $invoice->payment_term }}</p>
<hr>
<p style="font-size: 12px"><strong>Generated On:</strong> {{ $invoice->created_at->format('M d, Y H:i a') }}</p>
<p style="font-size: 12px"><strong>Invoice Number</strong> {{ $invoice->id }}</p>
<hr>
<p style="font-size: 12px"><strong>Invoice to:</strong><br>{{ $invoice->tenant->company_name }}</p>
<hr>
## Invoice Details
<hr>
@component("mail::table")
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th align="left">#</th>
                <th align="left">Item</th>
                <th align="right">Amount</th>
                <th align="right">VAT</th>
                <th align="right">Total</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th align="left">1</th>
                <th align="left">Rent</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->rent + $invoice->common_area_charge) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format(($invoice->rent + $invoice->common_area_charge) * 0.16) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format(($invoice->rent + $invoice->common_area_charge) * 0.16  + ($invoice->rent + $invoice->common_area_charge)) }}</th>
            </tr>
            <tr>
                <th align="left">2</th>
                <th align="left">Service Charge</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->service_charge) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->service_charge * 0.16) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->service_charge * 0.16  + $invoice->service_charge) }}</th>
            </tr>
            <tr>
                <th align="left">3</th>
                <th align="left">Parking Charges</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->parking_charges) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->parking_charges * 0.16) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->parking_charges * 0.16  + $invoice->parking_charges) }}</th>
            </tr>
            <tr>
                <th align="left">5</th>
                <th align="left">Electricity Bill</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->electricity_bill) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->electricity_bill * 0.16) }}</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->electricity_bill * 0.16  + $invoice->parking_charges) }}</th>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th align="left"></th>
                <th align="left">Total</th>
                <th align="right">{{ number_format($invoice->amount_payable)  }}</th>
                <th align="right">{{ number_format($invoice->amount_payable * 0.16) }}</th>
                <th align="right">{{ number_format($invoice->amount_payable * 0.16 + $invoice->amount_payable) }}</th>
            </tr>
        </tfoot>
    </table>
@endcomponent
@component("mail::button",  ['url' => url('downloads/invoice', $invoice->id)])
    Download Details
@endcomponent

<p style="font-size: 12px;font-weight: bold;">
    Payable via Cheque to "TRANCEWOOD LTD - TRANCE TOWERS"<br>
    PIN NO. - P051167631Y<br>
    VAT NO. - 0143687C<br>
</p>
Thank you for your business,<br>
{{ config('app.name') }}<br>
@endcomponent
