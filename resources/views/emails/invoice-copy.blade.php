@component('mail::message')
    <div class="logo" style="height: 70px;margin: 20px 0;">
        <hr>
        <img src="{{ get_asset("images/logo.png") }}" style="display: block; margin: 0 auto;" alt="trancewood logo">
        <hr>
    </div>
    # Invoice for {{ $invoice->tenant ? $invoice->tenant->name : 'N/A' }}
    <p style="font-size: 12px"><strong>Generated On:</strong> {{ $invoice->created_at->format('M d, Y H:i a') }}</p>
    <p style="font-size: 12px"><strong>Invoice Number</strong> {{ $invoice->id }}</p>
    <hr>
    <p style="font-size: 12px"><strong>Bill to:</strong><br>{{ $invoice->tenant->company_name }}</p>
    <hr>
    ## Invoice Details
    <hr>
@component("mail::table")
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th align="left">Floor</th>
                <th align="right" style="font-weight: normal">{{ $invoice->floor_level }}</th>
            </tr>
            <tr>
                <th align="left">Building</th>
                <th align="right" style="font-weight: normal">{{ $invoice->property_name }}</th>
            </tr>
            <tr>
                <th align="left">Office</th>
                <th align="right" style="font-weight: normal">{{ $invoice->unit_number }}</th>
            </tr>
            <tr>
                <th align="left">Area Rented <small>(sq m)</small></th>
                <th align="right" style="font-weight: normal">{{ $invoice->rented_area }}</th>
            </tr>
            <tr>
                <th align="left">Common Area <small>(sq m)</small></th>
                <th align="right" style="font-weight: normal">{{ $invoice->common_area }}</th>
            </tr>
            <tr>
                <th align="left">Parking Spots</th>
                <th align="right" style="font-weight: normal">{{ $invoice->parking_spots }}</th>
            </tr>
            <tr>
                <th align="left">Total Area <small>(sq m)</small></th>
                <th align="right" style="font-weight: normal">{{ $invoice->total_area }}</th>
            </tr>
        </thead>
    </table>
@endcomponent
@component("mail::table")
    <table class="table table-sm table-bordered">
        <thead>
        <tr>
            <th align="left">Interest Rate</th>
            <th align="right" style="font-weight: normal">{{ $invoice->rate_of_contract }}%</th>
        </tr>
        <tr>
            <th align="left">Term of lease</th>
            <th align="right" style="font-weight: normal">{{ $invoice->period_of_contract }} Months</th>
        </tr>
        <tr>
            <th align="left">Payment Term</th>
            <th align="right" style="font-weight: normal">{{ $invoice->payment_term }}</th>
        </tr>
        </thead>
    </table>
@endcomponent
@component("mail::table")
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th align="left">Interest</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->interest) }} Ksh</th>
            </tr>
            <tr>
                <th align="left">Common Area Charge</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->common_area_charge) }} Ksh</th>
            </tr>
            <tr>
                <th align="left">Service Charge</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->service_charge) }} Ksh</th>
            </tr>
            <tr>
                <th align="left">Rent</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->rent) }} Ksh</th>
            </tr>
            <tr>
                <th align="left">Parking Fees <small>(@ {{ $invoice->tenant->parking_spot_rent }} Ksh/spot)</small></th>
                <th align="right" style="font-weight: normal">{{ $invoice->parking_charges }}</th>
            </tr>
            <tr>
                <th align="left">Electricity Usage</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->electricity_usage) }} Units</th>
            </tr>
            <tr>
                <th align="left">Electricity Bill</th>
                <th align="right" style="font-weight: normal">{{ number_format($invoice->electricity_bill) }} Ksh <small>@ {{ $invoice->electricity_rate  }} Ksh/ unit</small></th>
            </tr>
            <tr>
                <th align="left">Total</th>
                <th align="right" style="font-weight: normal;">{{ number_format($invoice->amount_payable) }} Ksh</th>
            </tr>
        </thead>
    </table>
@endcomponent
@component("mail::button",  ['url' => url('downloads/invoice', $invoice->id)])
    Download Invoice
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
