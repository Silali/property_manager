<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ get_asset('annex/assets/css/bootstrap.min.css') }}">
    <title>Document</title>
    <style>
        body {
            font-size:12px !important;
        }
        h1 {
            font-size: 24px !important;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        }
    </style>
</head>
<body>
    <table class="table table-sm inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="margin-bottom:40px;margin-top:40px">
        <tr>
            <td style="width: 1%">
                <img src="{{ get_asset("images/logo.png") }}" style="display: block; height:30px;margin: 0 auto;" alt="trancewood logo">
            </td>
        </tr>
    </table>
    @yield("content")
</body>
</html>