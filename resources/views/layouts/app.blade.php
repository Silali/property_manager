<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield("title")</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="{{ get_asset('images/favicon.png') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link href="{{ get_asset('annex/assets/plugins/morris/morris.css') }}" rel="stylesheet">
    <link href="{{ get_asset('annex/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ get_asset('annex/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ get_asset('annex/assets/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ get_asset('annex/assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ get_asset('annex/assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    @yield("css")
    <style>
        .btn-xs {
            padding: 2px 6px;
            font-size: 12px;
            line-height: 1.8;
            border-radius: 3px;
        }
        .avatar-circle {
            width: 36px;
            height: 36px;
            background-color: white;
            text-align: center;
            color: #0b0b0b;
            border-radius: 50%;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
        }
        .initials {
            position: relative;
            top: -15px; /* 25% of parent */
            font-size: 18px; /* 50% of parent */
            line-height: 18px; /* 50% of parent */
            color: #5B6BE8;
            font-family:"Rubik", sans-serif;
            font-weight: bold;
        }
        .loading-spinner {
            width: 40px;
            height: 40px;

            position: relative;
            margin: 100px auto;
        }
        .double-bounce1, .double-bounce2 {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #333;
            opacity: 0.6;
            position: absolute;
            top: 0;
            left: 0;

            -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
            animation: sk-bounce 2.0s infinite ease-in-out;
        }
        .double-bounce2 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }
        @-webkit-keyframes sk-bounce {
            0%, 100% { -webkit-transform: scale(0.0) }
            50% { -webkit-transform: scale(1.0) }
        }

        @keyframes sk-bounce {
            0%, 100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0);
            } 50% {
                  transform: scale(1.0);
                  -webkit-transform: scale(1.0);
              }
        }
    </style>
</head>
    <body class="fixed-left">
    <div id="app">
        @yield("modals")
        <div id="">
        @include("_partials.sidebar")
        <div class="content-page">
            <div class="content">
                <div class="topbar">
                    <nav class="navbar-custom">
                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left waves-light waves-effect">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                        </ul>
                        <ul class="list-inline float-right mb-0">
                            <li class="list-inline-item dropdown notification-list">
                                <a class="nav-link arrow-none waves-effect" href="#"  style="color: #fff;" role="button" onclick="event.preventDefault();document.getElementById('logoutForm').submit()">
                                    <strong><i class="fa fa-power-off"></i></strong>
                                    <form action="{{ url('logout') }}" id="logoutForm" style="display: none;" method="post">
                                        {{ csrf_field() }}
                                    </form>
                                </a>
                            </li>
                            <li class="list-inline-item dropdown notification-list pt-2 pb-2 show">
                                <a class="nav-link arrow-none waves-effect nav-user dropdown"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href='{{ url("user/".Auth::id()."/profile")}}'>
                                    <div class="avatar-circle pull-right">
                                        <span class="initials">{{ Auth::user()->initials }}</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>

                    </nav>
                </div>
                <div class="page-content-wrapper ">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">@yield("title")</h4>
                                </div>
                            </div>
                        </div>
                        @yield("content")
                    </div>
                </div>
            </div>
            <footer class="footer">
                Design by <a href="http://symatechlabs.com/"><strong>Symatech</strong> Labs</a>
            </footer>

        </div>
    </div>
</div>
<!-- END wrapper -->
<!-- jQuery  -->
<script src="{{ get_asset('annex/assets/js/jquery.min.js') }}"></script>
<script src="{{ get_asset('js/app.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/popper.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/modernizr.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/detect.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/fastclick.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/waves.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.scrollTo.min.js') }}"></script>

<script src="{{ get_asset('annex/assets/plugins/skycons/skycons.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/plugins/raphael/raphael-min.js') }}"></script>
<!-- Required datatable js -->
<script src="{{ get_asset('annex/assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<!-- App js -->
<script src="{{ get_asset('annex/assets/js/app.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
    @yield("chart")
    @yield('js')
{!! Toastr::render() !!}
</body>
</html>