<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>@yield("title", 'Property Manager')</title>
    <meta content="Admin Dashboard" name="description" />
    <meta content="Mannatthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ get_asset('annex/assets/images/favicon.ico') }}">

    <link href="{{ get_asset('annex/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ get_asset('annex/assets/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ get_asset('annex/assets/css/style.css') }}" rel="stylesheet" type="text/css">

</head>


<body class="fixed-left">
<div style="background:#EFF3F6;
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;"></div>
<!-- Begin page -->
<div class="wrapper-page">
    <h3 class="text-center mt-0 m-b-15">
        <a href="{{ url('/') }}" class="logo logo-admin" style="font-size: 16px">
            <img src="{{ get_asset('images/logo.png') }}" alt="">
        </a>
    </h3>

    <div class="card" style="border: 1px solid rgba(0,0,0,0.12)">

        <div class="card-body">
            <div class="p-3">
                @yield("content")
            </div>

        </div>
    </div>
</div>


<!-- jQuery  -->
<script src="{{ get_asset('annex/assets/js/jquery.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/popper.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/modernizr.min.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/detect.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/fastclick.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.blockUI.js') }}"></script>
<script src="a{{ get_asset('annex/ssets/js/waves.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ get_asset('annex/assets/js/jquery.scrollTo.min.js') }}"></script>

<!-- App js -->
<script src="assets/js/app.js"></script>

</body>
</html>