@component('mail::message')
# Monthly Report

Generated on: {{ $data->generated_on }}

@component("mail::table")
    <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th align="left">Total No. of Offices</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->offices->total) }}</th>
            </tr>
            <tr>
                <th align="left">Unoccupied Offices</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->offices->unoccupied) }}</th>
            </tr>
            <tr>
                <th align="left">Total Parking Spots</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->parking->total) }}</th>
            </tr>
            <tr>
                <th align="left">Unoccupied Parking Spots</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->parking->unoccupied) }}</th>
            </tr>
            <tr>
                <th align="left">Expected Rent</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->rent->total) }}</th>
            </tr>
            <tr>
                <th align="left">Rent Collected</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->rent->paid) }}</th>
            </tr>
            <tr>
                <th align="left">Rent to Collect</th>
                <th align="right" style="font-weight: normal">{{ number_format($data->rent->to_collect) }}</th>
            </tr>
        </thead>
    </table>
@endcomponent




Thanks,<br>
{{ config('app.name') }}
@endcomponent
