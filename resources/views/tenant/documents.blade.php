@extends("layouts.app")
@section("content")
    <div class="row">
        <div class="col-12">
            @if($fix)
                <div class="alert alert-danger col-12 text-center">
                    There's a problem with documents <a href="{{ url('tenants/fix/documents') }}"><strong> fix it now</strong></a>
                </div>
            @endif
            <div class="card m-t-30">
                <div class="card-header" id="upload">
                    <document-uploader
                            upload-url="{{ url('tenants/documents/upload', $tenant->id) }}"
                    ></document-uploader>
                </div>
                <div class="card-body">
                    @if(count($documents) > 0)
                        <table class="table table-sm table-striped table-bordered">
                            <thead>
                            <tr>
                                <th align="center">#</th>
                                <th>Name</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($documents as $key => $document)
                                <tr>
                                    <td align="center">{{ $key + 1 }}</td>
                                    <td>{{ $document->name }}</td>
                                    <td align="right">
                                        @if($tenant->hasMedia($document->tag))
                                            <a class="btn-success btn-xs btn" href="{{ url('tenants/documents/download/'. $tenant->id .'/'.$document->tag )  }}">View</a>
                                            <dropzone-opener
                                                    document-tag="{{ $document->tag }}"
                                                    inline-template>
                                                <a href="#upload" class="btn btn-outline-primary btn-xs" @click="openUploader">Update</a>
                                            </dropzone-opener>
                                        @else
                                            <dropzone-opener
                                                    document-tag="{{ $document->tag }}"
                                                    inline-template>
                                                <a href="#upload" class="btn btn-xs" @click="openUploader">Upload</a>
                                            </dropzone-opener>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
