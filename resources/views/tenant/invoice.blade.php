@extends("layouts.app")
@section("content")
    <div class="row">
        <div class="col-12 m-b-30 m-t-30">
            <div class="card">
                <div class="card-body invoice">
                    <div class="clearfix">
                        <div class="pull-left">
                            <a href="index.html" class="logo"><i class="fa fa-home"></i> PM</a>
                        </div>
                        <div class="pull-right">
                            <h6>Invoice : #
                                <strong>23654789</strong>
                            </h6>
                            <h6 class="pull-right">Date : 11/01/2018</h6>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">

                            <div class="pull-left mt-4">
                                <address>
                                    <strong>Twitter, Inc.</strong><br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                    (123) 456-7890
                                </address>
                            </div>
                            <div class="pull-right mt-4">
                                <p><strong>Payment Due on: </strong> March 15, 2015</p>
                                <p><strong>Order Status: </strong> <span class="badge badge-warning">Pending</span></p>
                                <p><strong>Order ID: </strong> #123456</p>
                            </div>
                        </div>
                    </div><!--end row-->

                    <div class="h-50"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table mt-4">
                                    <thead>
                                        <tr>
                                            <th>Property</th>
                                            <th>Floor Level</th>
                                            <th>Sq m Rented</th>
                                            <th>Unit Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $tenant->property_name }}</td>
                                            <td>{{ $tenant->floor }}</td>
                                            <td>{{ $tenant->area_rented }}</td>
                                            <td>{{ number_format(20000) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div><!--end row-->

                    <div class="row" style="border-radius: 0px;">
                        <div class="col-md-9">
                            <p><strong>Terms And Condition : </strong></p>
                            <ul>
                                <li><small>All accounts are to be paid within 7 days from receipt of invoice. </small></li>
                                <li><small>To be paid by cheque or credit card or direct payment online.</small></li>
                                <li><small> If account is not paid within 7 days the credits details supplied as confirmation<br> of work undertaken will be charged the agreed quoted fee noted above.</small></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <p class="text-right">Payment Term: <strong>{{ $tenant->payment_term }}</strong></p>
                            <p class="text-right">Rate: <strong>{{ $tenant->rate_of_contract }}%</strong></p>
                            <p class="text-right">VAT: 12.9%</p>
                            <hr>
                            <h4 class="text-right">$ 2930.00</h4>
                        </div>
                    </div><!--end row-->
                    <hr>
                </div>
            </div>
        </div>
    </div>
@endsection