@extends("layouts.app")
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ url('tenants/update', $tenant->id) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <fieldset {{ $tenant->deleted_at != null ? 'disabled' : ''}}>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="company_name">Company/Business Name</label>
                                        <input type="text" class="form-control" name="company_name" id="company_name" value="{{ set_old('company_name', $tenant->company_name) }}">
                                        @if($errors->has('company_name'))
                                            <span class="form-text text-danger">{{ $errors->first('company_name') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="nature_of_business">Nature of Business</label>
                                        <textarea class="form-control" name="nature_of_business" id="nature_of_business">{{ set_old('nature_of_business', $tenant->nature_of_business) }}</textarea>
                                        @if($errors->has('nature_of_business'))
                                            <span class="form-text text-danger">{{ $errors->first('nature_of_business') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Operating Hours <small class="text-danger oh-range-error"></small></label>
                                        <div id="opsHours" class="input-group">
                                            <input type="text" class="time start form-control" value="{{ $tenant->oh_start }}"/>
                                            <input type="text" class="time end form-control" value="{{ $tenant->oh_end }}"/>
                                            <input type="hidden" name="operating_hours" id="operating_hours" value="{{ $tenant->operating_hours }}">
                                        </div>
                                        @if($errors->has('operating_hours'))
                                            <span class="form-text text-danger">{{ $errors->first('operating_hours') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label>Operating Hours <small>(Weekends and holidays)</small> <small class="text-danger woh-range-error"></small></label>
                                        <div id="wOpsHours" class="input-group">
                                            <input type="text" class="time start form-control" value="{{ $tenant->woh_end }}"/>
                                            <input type="text" class="time end form-control" value="{{ $tenant->woh_end }}"/>
                                            <input type="hidden" name="weekend_operating_hours" id="weekend_operating_hours" value="{{ $tenant->weekend_operating_hours }}">
                                        </div>
                                        @if($errors->has('weekend_operating_hours'))
                                            <span class="form-text text-danger">{{ $errors->first('weekend_operating_hours') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ set_old('name', $tenant->name) }}">
                                    @if($errors->has('name'))
                                        <span class="form-text text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="col">
                                    <label for="email">Email</label>
                                    <input type="email" class="form-control" name="email" id="email" value="{{ set_old('email', $tenant->email) }}">
                                    @if($errors->has('email'))
                                        <span class="form-text text-danger">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="col">
                                    <label for="phone">Phone</label>
                                    <input type="text" class="form-control" name="phone" id="phone" value="{{ set_old('phone', $tenant->phone) }}">
                                    @if($errors->has('phone'))
                                        <span class="form-text text-danger">{{ $errors->first('phone') }}</span>
                                    @endif
                                </div>
                            </div>
                            <floor-selector
                                    floors-url="{{ url('properties/get_floors') }}"
                                    property-value="{{ set_old('property', $tenant->property_id) }}"
                                    floor-value="{{ set_old('floor', $tenant->floor_id) }}"
                                    unit-rented-value="{{ set_old('unit', $tenant->unit_id) }}"
                                    units-url="{{ url('properties/floor_units/get') }}"
                                    inline-template>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <label for="property">Property</label>
                                        <select name="property" id="property" class="form-control" @change="getFloors" v-model="property">
                                            <option value="">Select Property</option>
                                            @foreach($properties as $property)
                                                <option value="{{ $property->id }}">{{ $property->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('property'))
                                            <span class="form-text text-danger">{{ $errors->first('property') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <label for="floor">Floor <small class="text-info" v-if="loading"><strong>loading options</strong></small></label>
                                        <select name="floor" id="floor" class="form-control" @change="handleFloorSelection" v-model="floorObject">
                                            <option value="">Select Floor</option>
                                            <option v-for="(option, i) in floorOptions" :value="option">@{{ option.level }}</option>
                                        </select>
                                        <input type="hidden" v-model="floor" name="floor">
                                        @if($errors->has('floor'))
                                            <span class="form-text text-danger">{{ $errors->first('floor') }}</span>
                                        @endif
                                    </div>
                                    <div class="col-md-4">
                                        <label for="unit">Office <small class="text-info text-sm-right" v-if="floorSelected"><strong>@{{ availableUnits }}</strong> units available</small></label>
                                        <select id="unit" :readonly="!floorSelected" v-model="unitObject" class="form-control" @change="handleUnitSelection">
                                            <option value="">Select Office</option>
                                            <option v-for="(option, i) in unitOptions" :value="option">@{{ option.unit_number }}</option>
                                        </select>
                                        <input type="hidden" v-model="unit" name="unit">
                                        @if($errors->has('unit'))
                                            <span class="form-text text-danger">{{ $errors->first('unit') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </floor-selector>
                            <div class="form-group">
                                <label for="parking_spots">Parking Spots</label>
                                <select type="text" class="form-control" name="parking_spots[]" id="parking_spots" multiple {{ $tenant->deleted_at != null ? 'disabled' : '' }}>
                                    @foreach($tenant->parking_spots as $spot)
                                        <option value="{{ $spot->id }}" selected>{{ $spot->number }}</option>
                                    @endforeach
                                    @foreach($spots as $spot)
                                        <option value="{{ $spot->id }}">{{ $spot->number }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('parking_spots'))
                                    <span class="form-text text-danger">{{ $errors->first('parking_spots') }}</span>
                                @endif
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="id_document">ID Document</label>
                                    <select name="id_document" id="id_document" class="form-control">
                                        <option value="">Select Document </option>
                                        @foreach($id_documents as $document)
                                            <option value="{{ $document->id }}" {{ set_old_option('id_document', str_replace(' ', '_', strtolower($tenant->id_document)), $document->slug) }}>{{ $document->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('id_document'))
                                        <span class="form-text text-danger">{{ $errors->first('id_document') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="id_number">ID Number</label>
                                    <input type="text" class="form-control" name="id_number" id="id_number" value="{{ set_old('id_number', $tenant->id_number) }}">
                                    @if($errors->has('id_number'))
                                        <span class="form-text text-danger">{{ $errors->first('id_number') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="id_file">ID File</label>
                                    <input type="file" class="form-control" name="id_file" id="id_file" accept="image/*, application/pdf">
                                    @if($errors->has('id_file'))
                                        <span class="form-text text-danger">{{ $errors->first('id_file') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="start_of_lease">Start of Lease (Date)</label>
                                    <input type="text" class="form-control" name="start_of_lease" id="start_of_lease" value="{{ set_old('start_of_lease', \Carbon\Carbon::parse($tenant->start_of_lease)->format('m/d/Y')) }}">
                                    @if($errors->has('start_of_lease'))
                                        <span class="form-text text-danger">{{ $errors->first('start_of_lease') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="period_of_contract">Period of Contract (Months)</label>
                                    <input type="number" class="form-control" name="period_of_contract" id="period_of_contract" value="{{ set_old('period_of_contract', $tenant->period_of_contract) }}">
                                    @if($errors->has('period_of_contract'))
                                        <span class="form-text text-danger">{{ $errors->first('period_of_contract') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label for="rent">Rent <small>per sq m</small></label>
                                    <input type="number" class="form-control" name="rent" id="rent" value="{{ set_old('rent', $tenant->rent) }}">
                                    @if($errors->has('rent'))
                                        <span class="form-text text-danger">{{ $errors->first('rent') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="common_area_rent">Common Area Rent <small>per sq m</small></label>
                                    <input type="number" class="form-control" name="common_area_rent" id="common_area_rent" value="{{ set_old('common_area_rent', $tenant->ca_rent) }}">
                                    @if($errors->has('common_area_rent'))
                                        <span class="form-text text-danger">{{ $errors->first('common_area_rent') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    <label for="parking_spot_rent">Parking Spot Rent <small>per sq m</small></label>
                                    <input type="number" class="form-control" name="parking_spot_rent" id="parking_spot_rent" value="{{ set_old('parking_spot_rent', $tenant->parking_spot_rent) }}">
                                    @if($errors->has('common_area_rent'))
                                        <span class="form-text text-danger">{{ $errors->first('parking_spot_rent') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label for="payment_term">Payment Terms</label>
                                    <select name="payment_term" id="payment_term" class="form-control">
                                        <option value="">Select a Term</option>
                                        @foreach($payment_terms as $term)
                                            <option value="{{ $term->id }}" {{ set_old_option('payment_term', $tenant->payment_term_id, $term->id) }}>{{ $term->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('payment_term'))
                                        <span class="form-text text-danger">{{ $errors->first('payment_term') }}</span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="rate_of_contract">Rate of Contract (%)</label>
                                    <select name="rate_of_contract" id="rate_of_contract" class="form-control">
                                        <option value="">Select a Rate</option>
                                        @foreach($interest_rates as $rate)
                                            <option value="{{ $rate->id }}" {{ set_old_option('rate_of_contract', $tenant->rate_of_contract_id, $rate->id) }}>{{ $rate->rate }}%</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('rate_of_contract'))
                                        <span class="form-text text-danger">{{ $errors->first('rate_of_contract') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                @if($tenant->deleted_at == null)
                                    <button class="btn btn-primary pull-left" type="submit">Submit</button>
                                    <delete-item
                                            delete-url='{{ url('tenants/delete', $tenant->id) }}'
                                            message="continue? This will terminate tenancy and vacate the Office {{ $tenant->unit->unit_number }}"
                                            inline-template>
                                            <a href="#" class="btn btn-sm btn-danger  pull-right" @click.prevent="deleteItem">End Tenancy</a>
                                    </delete-item>
                                @endif
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ get_asset('plugins/datepair/datepair.min.js') }}"></script>
    <script src="{{ get_asset('plugins/datepair/jquery.timepicker.min.js') }}"></script>
    <script src="{{ get_asset('annex/assets/plugins/select2/select2.min.js') }}"></script>
    <script src="{{ get_asset('plugins/input-mask/jquery.inputmask.bundle.js') }}"></script>
    <script>
        function setupTimePicker() {
            $('#start_of_lease').datepicker({
                autoclose: true
            });
            $('#oh .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:ia'
            });
            $('#w-oh .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:ia'
            });
            $('#opsHours .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:ia'
            });
            var opsHoursEl = document.getElementById('opsHours');
            var datepair = new Datepair(opsHoursEl);

            $('#wOpsHours .time').timepicker({
                'showDuration': true,
                'timeFormat': 'g:ia'
            });
            var wOpsHoursEl = document.getElementById('wOpsHours');
            var datepair = new Datepair(wOpsHoursEl);
            $('#opsHours').on('rangeSelected', function(){
                var start = $('#opsHours').find('.start').val();
                var end = $('#opsHours').find('.end').val();
                var range = start + " - " + end;
                $('#opsHours').find('#operating_hours').val(range);
            }).on('rangeIncomplete', function(){
                $('.oh-range-error').text('Incomplete range');
            }).on('rangeError', function(){
                $('.oh-range-error').text('Invalid range');
            });
            $('#wOpsHours').on('rangeSelected', function(){
                var start = $('#wOpsHours').find('.start').val();
                var end = $('#wOpsHours').find('.end').val();
                var range = start + " - " + end;
                $('#wOpsHours').find('#weekend_operating_hours').val(range);
            }).on('rangeIncomplete', function(){
                $('.woh-range-error').text('Incomplete range');
            }).on('rangeError', function(){
                $('.woh-range-error').text('Invalid range');
            });
        }
        $(document).ready(function(){
            setupTimePicker();
            $('#parking_spots').select2();
            $('#phone').inputmask("9999-999-999")
        });
    </script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ get_asset('plugins/datepair/jquery.timepicker.css') }}">
    <link rel="stylesheet" href="{{ get_asset('annex/assets/plugins/select2/select2.min.css') }}">
@endsection