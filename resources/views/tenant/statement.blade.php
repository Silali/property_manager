@extends("layouts.app")
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-5">
                <div class="card-body" style="padding: 0.5rem">
                    <form method="get" action="{{ url('tenants/statements', $tenant->id) }}">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <div>
                                    <label class="mb-1">
                                        <small><strong>date generated</strong></small>
                                    </label>
                                </div>
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control" name="from" placeholder="From Date" value="{{ set_filter_value($filter, 'from') }}">
                                    <input type="text" class="form-control" name="to" placeholder="To Date" value="{{ set_filter_value($filter, 'from') }}">
                                </div>
                            </div>
                            <div class="col-auto mt-4">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <button type="submit" name="pdf" value="1" class="btn btn-danger">PDF</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-body">
                    @if(count($statement) > 0)
                        <table class="table" >
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Receipt/Invoice No</th>
                                <th>Date </th>
                                <th>Credit</th>
                                <th>Debit</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody class="table-sm">
                                @foreach($statement as $key => $record)
                                    <tr class="{{ $record->invoice_id != null ? 'table-secondary' : '' }}">
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $record->invoice_id == null ? "receipt #$record->receipt_id" : "invoice #$record->invoice_id" }}</td>
                                        <td>{{ $record->date }}</td>
                                        <td>{{ $record->credit }}</td>
                                        <td>{{ $record->debit }}</td>
                                        <td>{{ $record->balance }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script src="{{ get_asset('annex/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
        jQuery('#date-range').datepicker({
            toggleActive: true
        });
    </script>
@endsection