@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-5">
                <div class="card-body" style="padding: 0.5rem">
                    <form method="get" action="{{ url('tenants') }}">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <select name="property" id="property" class="form-control">
                                    <option value="all">All Properties</option>
                                    @foreach($properties as $property)
                                        <option value="{{ $property->id }}" {{ set_current_param($filter, 'property', $property->id) }}>{{ $property->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-auto">
                                <select name="rate" id="rate" class="form-control">
                                    <option value="all">All Rates</option>
                                    @foreach($rates as $rate)
                                        <option value="{{ $rate->id }}" {{ set_current_param($filter, 'rate', $rate->id) }}>{{ $rate->rate }}%</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-auto">
                                <select name="term" id="term" class="form-control">
                                    <option value="all">All Payment Terms</option>
                                    @foreach($terms as $term)
                                        <option value="{{ $term->id }}"  {{ set_current_param($filter, 'term', $term->id) }}>{{ $term->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary">Filter</button>
                                <button type="submit" name="excel" value="1" class="btn btn-success">Excel</button>
                                <button type="submit" name="pdf" value="1" class="btn btn-danger">PDF</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                    <a href="{{ url('tenants/create') }}" class="btn btn-sm btn-primary pull-right">Add Tenant</a>
                </div>
                <div class="card-body">
                    @if(count($tenants) > 0)
                        <table class="table table-condensed table-striped" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>ID Number</th>
                                <th>Property</th>
                                <th>Office</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($tenants as $key => $tenant)
                                <tr>
                                    <td align="center">{{ $key + 1 }}</td>
                                    <td>{{ $tenant->name }} @if($tenant->deleted_at != null)<i class="text-danger fa fa-times-circle pull-right"></i>@endif</td>
                                    <td>{{ $tenant->phone }}</td>
                                    <td>{{ $tenant->id_number }}</td>
                                    <td>{{ (bool)$tenant->property ? $tenant->property->name : 'N/A' }}</td>
                                    <td>{{ (bool) $tenant->unit ? $tenant->unit->unit_number : 'N/A' }}</td>
                                    <td align="right">
                                        <div class="dropleft d-inline">
                                            <button class="btn btn-xs btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                View
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a href="{{ url('tenants/view', $tenant->id) }}" class="dropdown-item"><small>Tenant</small></a>
                                                <a class="dropdown-item" href="{{ url('tenants/documents', $tenant->id) }}"><small>Tenant Documents</small></a>
                                                <a class="dropdown-item" href="{{ url('tenants/statements', $tenant->id) }}"><small>Statements</small></a>
                                                <modal-opener
                                                        modal-name="elec-bill-editor"
                                                        :row-data="{{ json_encode(['bill' => $tenant->electricity_bill, 'tenant_id' => $tenant->id ]) }}"
                                                        inline-template>
                                                    <a class="dropdown-item" href="#" @click="show"><small>Add Electricity Bill</small></a>
                                                </modal-opener>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <elec-bill-editor
    name="elec-bill-editor"
    create-url="{{ url('tenants/electricity_bill/create') }}"
    get-url="{{ url('tenants/electricity_bill/get') }}"></elec-bill-editor>
@endsection