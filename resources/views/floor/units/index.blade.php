@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }} <small>{{ $floor->undesignated_area }} Sq m undesignated</small></h4>
                    <modal-opener
                            modal-name="floor-unit-editor"
                            :edit-mode="false"
                            inline-template>
                        <button class="btn btn-sm btn-primary pull-right" @click="show">Add Office</button>
                    </modal-opener>
                </div>
                <div class="card-body">
                    @if(count($units) > 0)
                        <table class="table table-condensed" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Number</th>
                                <th>Area</th>
                                <th>Tenant</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($units as $key => $unit)
                                <tr>
                                    <th>{{ $key + 1 }}</th>
                                    <td>{{ $unit->unit_number }}</td>
                                    <td>{{ number_format($unit->area) }}</td>
                                    <td>
                                        @if((bool) $unit->tenant)
                                            {{ $unit->tenant->name }}
                                        @else
                                            <span class="badge badge-dark badge-pill">unoccupied</span>
                                        @endif
                                    </td>
                                    <td align="right">
                                        <modal-opener
                                                modal-name="floor-unit-editor"
                                                :row-data="{{ json_encode($unit) }}"
                                                inline-template>
                                            <button class="btn btn-xs btn-primary" @click="show"><strong>Edit</strong></button>
                                        </modal-opener>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <floor-unit-editor
            update-url="{{ url('properties/floor_units/update') }}"
            create-url="{{ url('properties/floor_units/store', $floor->id) }}">
    </floor-unit-editor>
@endsection