@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                    <modal-opener
                            modal-name="floor-editor"
                            :edit-mode="false"
                            inline-template>
                        <button class="btn btn-sm btn-primary pull-right" @click="show">Add Floor</button>
                    </modal-opener>
                </div>
                <div class="card-body">
                    @if(count($floors) > 0)
                        <table class="table table-condensed" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Level</th>
                                <th>Rentable Area <small>(Sq Mtrs)</small></th>
                                <th>Rented Area <small>(Sq Mtrs)</small></th>
                                <th>Common Area <small>(Sq Mtrs)</small></th>
                                <th align="right" style="min-width: 276px"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($floors as $key => $floor)
                                    <tr>
                                        <th>{{ $key + 1 }}</th>
                                        <td>{{ $floor->level }}</td>
                                        <td>{{ number_format($floor->total_rentable_area) }}</td>
                                        <td>{{ number_format($floor->units()->sum('area')) }}</td>
                                        <td>{{ number_format($floor->common_area) }}</td>
                                        <td align="right">
                                            <modal-opener
                                                    modal-name="property-chart"
                                                    :row-data="{{ json_encode(['id' => $floor->id]) }}"
                                                    inline-template>
                                                <button class="btn btn-outline-secondary btn-xs" @click.prevent="show">Projections</button>
                                            </modal-opener>
                                            <a href="{{ url('properties/parking_spots', $floor->id) }}" class="btn btn-xs btn-outline-primary">Parking Spots</a>
                                            <a href="{{ url('properties/floor_units', $floor->id) }}" class="btn btn-xs btn-outline-success">Offices</a>
                                            <modal-opener
                                            modal-name="floor-editor"
                                            :row-data="{{ json_encode($floor) }}"
                                            inline-template>
                                                <button class="btn btn-xs btn-primary" @click="show">Edit</button>
                                            </modal-opener>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <floor-editor
    update-url="{{ url('properties/floors/update') }}"
    create-url="{{ url('properties/floors/store', $property_id) }}"
    ></floor-editor>
    <floor-projection-chart
            data-url="{{ url('floors/get_projection_data') }}">
    </floor-projection-chart>
@endsection