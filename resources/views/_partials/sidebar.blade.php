<div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
        <i class="ion-close"></i>
    </button>
    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="{{ url('') }}" class="logo"><img src="{{ get_asset('images/logo.png') }}" height="24" alt="logo"></a>
        </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="{{ url('dashboard') }}" class="waves-effect {{ set_active_class('dashboard', 2) }}">
                        <i class="mdi mdi-airplay"></i>
                        <span> Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('properties') }}" class="waves-effect {{ set_active_class('properties') }}">
                        <i class="mdi mdi-domain"></i>
                        <span> Properties</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('tenants') }}" class="waves-effect {{ set_active_class('tenants') }}">
                        <i class="mdi mdi-account"></i>
                        <span> Tenants</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url('payments') }}" class="waves-effect {{ set_active_class('payments') }}">
                        <i class="mdi mdi-credit-card"></i>
                        <span> Payments</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('bills') }}" class="waves-effect {{ set_active_class('bills') }}">
                        <i class="mdi mdi-calendar"></i>
                        <span> Bills</span>
                    </a>
                </li>
                 <li>
                    <a href="{{ url('users') }}" class="waves-effect {{ set_active_class('users') }}">
                        <i class="mdi mdi-account"></i>
                        <span> Users</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('settings') }}" class="waves-effect {{ set_active_class('settings') }}">
                        <i class="mdi mdi-settings"></i>
                        <span> Settings</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>