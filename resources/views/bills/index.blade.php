@extends("layouts.app")
@section("title"){{ $title }}@endsection
@section("content")
    <div class="row mb-4">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <a class="round bg-primary" href="javascript:void(0)">
                        <i class="fa fa-money text-white"></i>
                    </a>
                    <h2 class="m-t-10 m-b-0">{{ number_format($amount_to_be_paid) }} <small>Ksh</small></h2>
                    <h6>Amount to be paid</h6>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <a class="round bg-primary" href="javascript:void(0)">
                        <i class="fa fa-thumbs-o-up text-white"></i>
                    </a>
                    <h2 class="m-t-10 m-b-0">{{ number_format($amount_paid) }} <small>Ksh</small></h2>
                    <h6>Amount Paid</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-5">
                <div class="card-body" style="padding: 0.5rem">
                    <form method="get" action="{{ url('bills') }}" class="form-inline">
                        <label for="month"></label>
                        <select name="month" id="month" class="mr-2 form-control">
                            @foreach($months as $month)
                                <option value="{{ $month['value'] }}" {{ $current_month === $month['value'] ? 'selected' : '' }}>{{ $month['name'] }}</option>
                            @endforeach
                        </select>
                        <label for="year"></label>
                        <select name="year" id="year" class="form-control">
                            @foreach($years as $year)
                                <option value="{{ $year }}" {{ $current_year === $year ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-primary ml-2">Filter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                </div>
                <div class="card-body">
                    @if(count($bills) > 0)
                        <table class="table table-sm table-striped" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Bill</th>
                                <th>Month</th>
                                <th>Cost</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bills as $key => $bill)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $bill->name }}</td>
                                    <td>{{ $bill->created_at->format('F') }}</td>
                                    <td>{{ $bill->estimated_cost }}</td>
                                    <td align="right">
                                       @if(! $bill->cleared)
                                            <modal-opener inline-template
                                                          modal-name="bill-clearance-editor"
                                                          row-data="{{ $bill->id }}">
                                                <button class="btn btn-xs btn-primary" @click.prevent="show">Clear</button>
                                            </modal-opener>
                                        @else
                                            <a href="{{ url($bill->document_url)  }}" target="_blank">See Document</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section("modals")
    <bill-clearance-editor
    create-url="{{ url('bills/clear') }}"></bill-clearance-editor>
@endsection