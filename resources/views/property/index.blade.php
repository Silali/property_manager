@extends("layouts.app")
@section("title")
    {{ $title }}
@endsection
@section("content")
    <div class="row">
        <div class="col-12">
            <div class="card m-b-5">
                <div class="card-body" style="padding: 0.5rem">
                    <form method="get" action="{{ url('properties') }}">
                            <div class="col-auto">
                                <button type="submit" name="excel" value="1" class="btn btn-success">Excel</button>
                                <button type="submit" name="pdf" value="1" class="btn btn-danger">PDF</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card m-b-30">
                <div class="card-header">
                    <h4 class="header-title pull-left mt-0">{{ $title }}</h4>
                    <modal-opener
                    modal-name="property-editor"
                    :row-data="{ name: '',location: '',floors: 1}"
                    :update-item="false"
                    inline-template>
                        <button class="btn btn-sm btn-primary pull-right" @click="show">Add Property</button>
                    </modal-opener>
                </div>
                <div class="card-body">
                    @if(count($properties) > 0)
                        <table class="table table-condensed table-striped" id="datatable">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Floors</th>
                                <th align="right"></th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($properties as $key => $property)
                                    <tr>
                                        <th>{{ $key + 1 }}</th>
                                        <td>{{ $property->name }}</td>
                                        <td>{{ $property->location }}</td>
                                        <td>{{ count($property->floors) }}</td>
                                        <td align="right">
                                            <modal-opener
                                                    modal-name="property-chart"
                                                    :row-data="{{ json_encode(['id' => $property->id]) }}"
                                                    inline-template>
                                                <button class="btn btn-outline-secondary btn-xs" @click.prevent="show">Projections</button>
                                            </modal-opener>
                                            <a href="{{ url("properties/$property->id/floors") }}" class="btn-success btn-xs btn">Floors</a>
                                            <modal-opener
                                            modal-name="property-editor"
                                            :row-data="{{ json_encode($property) }}"
                                            inline-template>
                                                <button class="btn btn-xs btn-primary" @click="show">Edit</button>
                                            </modal-opener>
                                            <delete-item
                                            delete-url='{{ url('properties/delete', $property->id) }}'
                                            inline-template>
                                                <button class="btn btn-xs btn-danger" @click="deleteItem">Delete</button>
                                            </delete-item>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-info" role="alert">
                            <strong>No records found</strong>
                        </div>
                    @endif
                </div>
            </div>
        </div> <!-- end col -->
    </div>
@endsection
@section('js')
    <script>
        function createItem(e) {
            e.preventDefault();
        }
        $(document).ready( function () {
            $('#datatable').DataTable();
        });
    </script>
@endsection
@section('css')

@endsection
@section('modals')
    <property-editor
    create-url="{{ url('properties/create') }}"
    update-url="{{ url('properties/update') }}"
    ></property-editor>
    <property-projection-chart
            data-url="{{ url('properties/get_projection_data') }}">
    </property-projection-chart>
@endsection