<?php

use Illuminate\Database\Seeder;

class SettingsTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $interest_rates = [
            ['rate' => 1],
            ['rate' => 12],
            ['rate' => 18],
        ];
        \App\InterestRate::truncate();
        foreach ($interest_rates as $interest_rate) {
            \App\InterestRate::create($interest_rate);
       }
        $id_documents = [
            ['name' => 'National ID', 'slug' => 'national_id'],
            ['name' => 'Passport', 'slug' => 'passport'],
            ['name' => 'Alien ID', 'slug' => 'alien_id'],
        ];
        \App\IdDocument::truncate();
        foreach ($id_documents as $id_document) {
            \App\IdDocument::create($id_document);
        }
        $payment_terms = [
            ['name' => 'Quarterly', 'period' => 3, 'slug' => 'quarterly'],
            ['name' => 'Monthly', 'period' => 1, 'slug' => 'monthly'],
            ['name' => 'Biannual', 'period' => 4, 'slug' => 'biannual'],
        ];
        \App\PaymentTerm::truncate();
        foreach ($payment_terms as $payment_term) {
            \App\PaymentTerm::create($payment_term);
        }
        $payment_methods = [
            ['name' => 'Cheque', 'slug' => 'cheque'],
            ['name' => 'Bank Slip', 'slug' => 'bank_slip']
        ];

        \App\PaymentMethod::truncate();
        foreach ($payment_methods as $payment_method) {
            \App\PaymentMethod::create($payment_method);
        }
    }
}
