<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \App\User::truncate();

        \App\User::create([
            'email' => 'rahulvekaria18@gmail.com',
            'password' => bcrypt('tranceproperty2018'),
            'name' => 'rahulvekaria18'
        ]);



        \App\User::create([
            'email' => 'rahul@trancewood.com',
            'password' => bcrypt('tranceproperty2018'),
            'name' => 'rahul'
        ]);
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    }
}
