<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_statements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('invoice_id')->nullable();
            $table->integer('receipt_id')->nullable();
            $table->text('balance');
            $table->string('credit')->default('0');
            $table->string('debit')->default('0');
            $table->timestamp('date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_statement');
    }
}
