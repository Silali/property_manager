<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalTenantsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->text('nature_of_business');
            $table->text('company_name');
            $table->text('operating_hours');
            $table->text('weekend_operating_hours');
            $table->integer('parking_spot_rent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenants', function (Blueprint $table) {
            $table->dropColumn('nature_of_business');
            $table->dropColumn('company_name');
            $table->dropColumn('operating_hours');
            $table->dropColumn('weekend_operating_hours');
            $table->dropColumn('parking_spot_rent');
        });
    }
}
