<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectricityBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electricity_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id')->index();
            $table->double('current_usage')->nullable();
            $table->double('previous_usage')->nullable();
            $table->integer('current_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electricity_bills');
    }
}
