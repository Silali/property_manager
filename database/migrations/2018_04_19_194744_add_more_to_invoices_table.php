<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreToInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->bigInteger('common_area_charge');
            $table->bigInteger('interest');
            $table->bigInteger('rent');
            $table->bigInteger('service_charge');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropColumn('common_area_charge');
            $table->dropColumn('interest');
            $table->dropColumn('rent');
            $table->dropColumn('service_charge');
        });
    }
}
