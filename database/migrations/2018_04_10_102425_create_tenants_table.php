<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('phone');
            $table->text('email');
            $table->integer('property_id')->index();
            $table->integer('floor_id')->index();
            $table->integer('unit_id')->index();
            $table->integer('rent');
            $table->text('id_document');
            $table->text('id_number');
            $table->integer('payment_term_id')->index();
            $table->integer('rate_of_contract_id')->index();
            $table->integer('period_of_contract');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
